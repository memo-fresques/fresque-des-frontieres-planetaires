---
fromCardId: '1'
toCardId: '7'
status: valid
---


Climate change is warming sea water. As a result, water expands, takes on more volume, and contributes to the rise of the oceans. This phenomenon was predominant until recently, it is now the melting of glaciers and ice caps which is taking over.
