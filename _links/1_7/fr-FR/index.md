---
fromCardId: '1'
toCardId: '7'
status: valid
---

Le changement climatique réchauffe l'eau des mers. De ce fait, l'eau se dilate, prend plus de volume, et contribue à la montée des océans. Ce phénomène était prépodérant jusqu'à peu, c'est maintenant la fonte des glaciers et des calottes glaciaires qui prend le dessus.
