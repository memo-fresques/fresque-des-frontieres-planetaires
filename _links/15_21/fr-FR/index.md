---
fromCardId: '15'
toCardId: '21'
status: valid
---
La deuxième grande cause de perte de biodiversité est l’exploitation directe des milieux. Sur les terres, cela se traduit par l’agriculture intensive via l’usage des produits phytosanitaires (pesticides, herbicides…) et de la simplification des milieux naturels qu’elle entraîne. 
En mer, c’est la surpêche qui est en cause.
