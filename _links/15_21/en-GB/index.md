---
fromCardId: '15'
toCardId: '21'
status: valid
---
The second major cause of biodiversity loss is direct exploitation of environments. On land, this translates into intensive agriculture through the use of phytosanitary products (pesticides, herbicides, etc.) and the simplification of natural environments that this entails.
At sea, overfishing is the cause.
