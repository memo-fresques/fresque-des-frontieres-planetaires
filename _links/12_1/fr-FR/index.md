---
fromCardId: '12'
toCardId: '1'
status: valid
---
Le méthane (CH4) et le protoxyde d'azote (N2O) sont les principaux gaz à effet de serre GES avec le CO2 en terme d'impact. Ils ont un PRG (potentiel de réchauffement global) supérieur au CO2 (28 pour le méthane et 265 pour le protoxyde d'azote, en prenant une référence de 100 ans), mais sont émis en quantités bien inférieures. 
