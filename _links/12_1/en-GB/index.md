---
fromCardId: '12'
toCardId: '1'
status: valid
---
Methane (CH4) and nitrous oxide (N2O) are the main GHG greenhouse gases along with CO2 in terms of impact. They have a GWP (global warming potential) higher than CO2 (28 for methane and 265 for nitrous oxide, taking a 100-year reference), but are emitted in much lower quantities.
