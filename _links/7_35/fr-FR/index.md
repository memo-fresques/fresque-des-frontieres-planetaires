---
fromCardId: '7'
toCardId: '35'
status: valid
---
Les populations installées dans de grandes métropoles côtières, dans des deltas, ou bien sur des îles, soit plus de 10% de la population mondiale, seront exposées à ce phénomène. 8 des 10 + grandes villes du monde se trouvent en zone  côtière, et 40% de la population US vit près d’une côte. Au risque de submersion s’ajoute celui d’inondations passagères qui menacent les infrastructures de villes comme New-York, Tokyo, Jakarta, Mumbai, Lagos ou Shanghai, très proches du niveau de la mer.
