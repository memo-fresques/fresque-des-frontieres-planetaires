---
fromCardId: '7'
toCardId: '35'
status: valid
---
Populations living in large coastal cities, in deltas, or on islands, i.e. more than 10% of the world population, will be exposed to this phenomenon. 8 of the 10 largest cities in the world are located in coastal areas, and 40% of the US population lives near a coast. In addition to the risk of submersion, there is the risk of temporary floods which threaten the infrastructure of cities like New York, Tokyo, Jakarta, Mumbai, Lagos or Shanghai, very close to sea level.
