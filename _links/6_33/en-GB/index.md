---
fromCardId: '6'
toCardId: '33'
status: valid
---
Climatic hazards can undermine the effectiveness of certain ecosystem services (megafires which destroy biodiversity habitats and which reduce the capacity to absorb CO2, floods which add salt water to groundwater).
