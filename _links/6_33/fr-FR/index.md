---
fromCardId: '6'
toCardId: '33'
status: valid
---
Les aléas climatiques peuvent porter atteinte à l'efficacité de certains services écosystémiques (mégafeux qui détruisent des habitats de biodiversité et qui diminuent la capacité d'absorption du CO2, submersions qui ajoutent de l'eau salée dans des nappes phréatiques).
