---
fromCardId: '14'
toCardId: '16'
status: valid
---
L'agriculture conventionnelle nécessite de grandes quantités d'engrais, notamment de l'azote synthétisé avec du gaz fossile, et du phosphore extrait de mines.
