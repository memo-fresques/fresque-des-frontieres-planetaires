---
fromCardId: '14'
toCardId: '16'
status: valid
---
Conventional agriculture requires large quantities of fertilizers, including nitrogen synthesized with fossil gas, and phosphorus extracted from mines.
