---
fromCardId: '7'
toCardId: '39'
status: optional
---
Les deltas sont des zones très fertiles. La montée du niveau des mers entraine la salinisation de ces zones agricoles, et a donc un impact sur les rendements agricoles.
