---
fromCardId: '7'
toCardId: '39'
status: optional
---
Deltas are very fertile areas. Rising sea levels lead to the salinization of these agricultural areas, and therefore have an impact on agricultural yields.
