---
fromCardId: '1'
toCardId: '34'
status: valid
---
The amount of oxygen that can be dissolved in water depends on the temperature: the warmer it is, the less oxygen in the air dissolves in the water. Climate change therefore limits the dissolution of oxygen in water.
