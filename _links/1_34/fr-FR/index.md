---
fromCardId: '1'
toCardId: '34'
status: valid
---
La quantité d'oxygène qui peut être dissoute dans l'eau dépend de la température : plus il fait chaud et moins l'oxygène de l'air se dissout dans l'eau. Le changement climatique limite donc la dissolution de l’oxygène dans l’eau.
