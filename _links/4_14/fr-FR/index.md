---
fromCardId: '4'
status: valid
toCardId: '14'
---

L'agriculture est l'une des premières activités humaines répondant à un besoin fondamental : se nourrir.
