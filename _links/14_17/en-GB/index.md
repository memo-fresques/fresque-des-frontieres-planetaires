---
fromCardId: '14'
toCardId: '17'
status: valid
---
Productivist agriculture/breeding accounts for 70% of drinking water withdrawals in the world (and up to 95% in certain developing countries!), industry (20%) and domestic consumption 10%.
The reason is the gigantic irrigation of our crops.
