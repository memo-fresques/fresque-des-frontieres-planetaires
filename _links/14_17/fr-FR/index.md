---
fromCardId: '14'
toCardId: '17'
status: valid
---
L’agriculture/élevage productiviste réalise 70% des prélèvements d’eau potable dans le monde (et jusqu’à 95% dans certains pays en développement !), l’industrie (20%) et la consommation domestique 10%.
La raison est l’irrigation gigantesque de nos cultures.
