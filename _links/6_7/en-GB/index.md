---
fromCardId: '6'
toCardId: '7'
status: optional
---
Cyclones can accentuate the rise in water levels through a local depression phenomenon.
