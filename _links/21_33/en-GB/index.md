---
fromCardId: '21'
toCardId: '33'
status: valid
---
“Less abundant and/or unhealthy biodiversity affects the effectiveness of the services provided by nature.
The services provided by nature are innumerable and essential to human life (pollination, creation of arable soil, water purification, regulation of material and energy flows, etc.). "
