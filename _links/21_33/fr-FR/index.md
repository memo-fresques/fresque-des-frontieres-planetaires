---
fromCardId: '21'
toCardId: '33'
status: valid
---
"Une biodiversité moins abondante et/ou en mauvaise santé affecte l'efficacité des services rendus par la nature.
Les services rendus par la nature sont innombrables et indispensables à la vie humaine (pollinisation, création de sols arables, épuration de l’eau, régulation des flux de matière et d’énergie…). "
