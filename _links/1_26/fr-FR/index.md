---
fromCardId: '1'
toCardId: '26'
status: valid
---
Avec l'augmentation de la température de l'air et de l'eau, l'évaporation augmente. A plus grande température, l'air peut contenir plus d'eau avant de précipiter (pluie). Le régime des pluies est donc affecté par le réchauffement climatique. Il y a une augmentation des événements de pluie extrêmes.
