---
fromCardId: '1'
toCardId: '26'
status: valid
---
With increasing air and water temperature, evaporation increases. At higher temperatures, the air can hold more water before precipitating (rain). The rainfall pattern is therefore affected by global warming. There is an increase in extreme rain events.
