---
fromCardId: '28'
toCardId: '30'
status: optional
---
If we consider that this map also includes glaciers, then there is indeed an impact on fresh water resources: the glaciers of the Hindu Kush and the Himalayas provide drinking water to 250 million people spread across 8 country of the world. By 2100, more than 2/3 of them could have melted, which will cause floods and then shortages of fresh water: the large rivers such as the Huang Hue (Yellow River), the Yangtze, the Brahmaputra, 'Indus and the Ganges will no longer carry much water in summer, which supply Bangladesh, Pakistan and northern India.
