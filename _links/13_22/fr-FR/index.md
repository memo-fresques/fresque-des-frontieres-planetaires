---
fromCardId: '13'
toCardId: '22'
status: valid
---
La destruction des habitats est une des cause du changement d'affectation des sols. C'est d'ailleurs l'indicateur utilisé pour le quantifier.
