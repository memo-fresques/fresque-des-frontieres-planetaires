---
fromCardId: '13'
toCardId: '22'
status: valid
---
The destruction of habitats is one of the causes of land use change. This is also the indicator used to quantify it.
