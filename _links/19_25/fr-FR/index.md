---
fromCardId: '19'
toCardId: '25'
status: valid
---
L’acidification altère les processus de calcification qui limitent le développement des coraux, des animaux à coquilles et du plancton qui est à la base de la chaîne alimentaire marine. Toute la biodiversité marine s’en trouve ainsi menacée.
