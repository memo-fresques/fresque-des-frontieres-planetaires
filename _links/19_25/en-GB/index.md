---
fromCardId: '19'
toCardId: '25'
status: valid
---
Acidification alters the calcification processes which limit the development of corals, shelled animals and plankton which are at the base of the marine food chain. All marine biodiversity is thus threatened.
