---
fromCardId: '26'
toCardId: '30'
status: valid
---
Fresh water resources depend partly on rainfall. Less rain can therefore lead to less fresh water resources in certain regions.
