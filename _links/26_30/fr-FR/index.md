---
fromCardId: '26'
toCardId: '30'
status: valid
---
Les ressources en eau douce dépendent en partie des pluies. Moins de pluies peu donc entrainer moins de ressource en eau douce sur certaines régions.
