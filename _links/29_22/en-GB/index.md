---
fromCardId: '29'
toCardId: '22'
status: valid
---
The savannahization of the Amazon would significantly reduce the world's forested area, and therefore would have an impact on land use by transforming a large forest area into savannah.
