---
fromCardId: '29'
toCardId: '22'
status: valid
---
La savanisation de l'Amazonie diminuerait de manière importante la surface boisée mondiale, et donc aurait un impact sur l'affectation des sols en transformant une grande zone forestière en savane.
