---
fromCardId: '5'
status: valid
toCardId: '11'
---

La combustion imparfaite des énergies fossiles entraine des émissions d'aérosols dans l'atmosphère.
