---
fromCardId: '23'
toCardId: '12'
status: optional
---
Nitrous oxide (N2O) emissions are directly linked to the disruption of the nitrogen cycle, which is one of the nine key areas defined by the Planetary Borders concept. The nitrogen cycle is a natural process that involves the different stages of fixation, assimilation, transformation and release of nitrogen into the environment.
