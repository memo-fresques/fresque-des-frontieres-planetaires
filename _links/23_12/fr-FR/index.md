---
fromCardId: '23'
toCardId: '12'
status: optional
---
Les émissions de protoxyde d'azote (N2O) sont directement liées à la perturbation du cycle de l'azote, qui est l'un des neuf domaines clés définis par le concept des Frontières Planétaires. Le cycle de l'azote est un processus naturel qui implique les différentes étapes de fixation, d'assimilation, de transformation et de libération de l'azote dans l'environnement.
