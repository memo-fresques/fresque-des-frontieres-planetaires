---
fromCardId: '13'
toCardId: '21'
status: valid
---
La première cause de déclin de biodiversité terrestre est le changement d'usage des sols (lié à la déforestation principalement et un peu de l’artificialisation des sols) de la faune et de la flore. Cela détruit les habitats des espèces.
