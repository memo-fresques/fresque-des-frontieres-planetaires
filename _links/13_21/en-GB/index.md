---
fromCardId: '13'
toCardId: '21'
status: valid
---
The primary cause of decline in terrestrial biodiversity is the change in land use (mainly linked to deforestation and a little to the artificialization of land) of fauna and flora. This destroys species habitats.
