---
fromCardId: '17'
toCardId: '24'
status: valid
---
Significant consumption or withdrawal of fresh water modifies the natural cycle, by delaying returns to the natural environment (storage), by moving volumes of water (pumping to one place and return to another), by modifying the constitution (temperature , pollution).
