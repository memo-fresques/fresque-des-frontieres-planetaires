---
fromCardId: '17'
toCardId: '24'
status: valid
---
Des consommations ou prélèvements importants d'eau douce modifient le cycle naturel, en retardant les restitutions au milieu naturel (stockage), en déplaçant des volumes d'eau (pompage à un endroit et restitution à un autre), en modifiant la constitution (température, pollutions).
