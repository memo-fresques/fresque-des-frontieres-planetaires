---
fromCardId: '11'
toCardId: '1'
status: valid
---
Some aerosols have a cooling effect, and others a warming effect (depending on their composition, their altitudes, and even the seasons), but overall, the radiative forcing of aerosols is negative and therefore has a cooling effect.
In fact, aerosols will (1) reflect the sun's rays directly and (2) contribute to the formation of clouds which will also reflect the sun's rays.
