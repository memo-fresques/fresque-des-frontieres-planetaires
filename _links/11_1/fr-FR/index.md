---
fromCardId: '11'
toCardId: '1'
status: valid
---
Certains aérosols ont un effet refroidissant, et d'autres un effet réchauffant (en fonction de leurs composition, de leurs altitudes, et même des saisons), mais au global, le forçage radiatif des aérosols est négatif et a donc un effet refroidissant.
En effet les aérosols vont (1) réfléchir les rayons du soleil directement et (2) contribuer à la formation des nuages qui vont eux aussi réfléchir les rayons du soleil.
