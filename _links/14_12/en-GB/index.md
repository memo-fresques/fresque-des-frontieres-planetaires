---
fromCardId: '14'
toCardId: '12'
status: valid
---
CH4: The digestion of ruminants (cattle, goats) releases methane via the enteric fermentation process. Organic matter is decomposed without oxygen. The same process takes place in rice fields.


N2O comes from the spreading of nitrogen fertilizers. The part of the fertilizer that does not go into the plant to make it grow oxidizes in the open air.
