---
fromCardId: '14'
toCardId: '12'
status: valid
---
CH4 : La digestion des ruminants (bovins, caprins) dégage du méthane via le processus de fermentation entérique. La matière organique est décomposée sans oxygène. Le même processus a lieu dans les rizières. 

Le N2O est issu de l'épandage d'engrais azotés. La partie des engrais qui ne va pas dans la plante pour la faire pousser s'oxyde à l'air libre.
