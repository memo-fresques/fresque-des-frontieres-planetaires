---
fromCardId: '26'
toCardId: '39'
status: valid
---
La perturbation du régime des pluies (moins de pluie ou pluies plus intenses) peut avoir un impact sur les rendements agricoles.
