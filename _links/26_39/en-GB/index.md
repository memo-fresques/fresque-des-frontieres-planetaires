---
fromCardId: '26'
toCardId: '39'
status: valid
---
Disruption of the rain pattern (less rain or more intense rain) can have an impact on agricultural yields.
