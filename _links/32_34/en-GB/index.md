---
fromCardId: '32'
toCardId: '34'
status: valid
---
"Eutrophication of aquatic environments is characterized by excessive growth of plants and algae due to the high availability of nutrients. The algae that thrive on these nutrients absorb large amounts of oxygen when they die and Their proliferation causes the impoverishment and then the death of the present aquatic ecosystem: it no longer benefits from the oxygen necessary to live, this phenomenon is called “asphyxiation of aquatic ecosystems”.
In 2003, a UN report estimated the number of dead zones in the oceans at 150; five years later, a study published by the Virginia Institute of Marine Sciences counted more than 400. Spread over 245,000 km2, these are mainly found in the South Pacific, the Baltic Sea, the coasts of Namibia or even in the Gulf of Mexico.
In these regions, many species cannot survive. According to the IPCC, deoxygenation could lead to a loss of 15% of the global biomass of marine animals by 2100.
