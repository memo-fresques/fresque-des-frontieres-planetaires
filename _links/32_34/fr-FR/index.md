---
fromCardId: '32'
toCardId: '34'
status: valid
---
"L’eutrophisation des milieux aquatiques est caractérisée par une croissance excessive des plantes et des algues due à la forte disponibilité des nutriments. Les algues qui se développent grâce à ces substances nutritives absorbent de grandes quantités d'oxygène, lorsqu'elles meurent et se décomposent. Leur prolifération provoque l'appauvrissement, puis la mort de l'écosystème aquatique présent : il ne bénéficie plus de l'oxygène nécessaire pour vivre, ce phénomène est appelé « asphyxie des écosystèmes aquatiques ».
En 2003, un rapport de l’ONU estimait à 150 le nombre de zones mortes dans les océans, cinq ans plus tard, une étude publiée par l’Institut de sciences marines de Virginie en dénombrait plus de 400. Réparties sur 245 000 km2, celles-ci se trouvent  principalement dans le Pacifique du sud, la mer Baltique, les côtes de Namibie ou encore dans le golfe de Mexico.
Dans ces régions, nombre d’espèces ne peuvent survivre. Selon le GIEC, la désoxygénation pourrait mener à une perte de 15% de la biomasse globale des animaux marins d’ici 2100."
