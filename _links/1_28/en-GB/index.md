---
fromCardId: '1'
toCardId: '28'
status: valid
---
Climate change leads to melting of ice caps (= very large glaciers).
The poles are the regions of the world where the impacts of climate change are most visible, temperatures are warming faster there and ice melting is accelerating.
A tipping point leading to the complete disappearance of Greenland and West Antarctica is not established, but not impossible.
We are not talking about glaciers here, even if they are also impacted by global warming.
