---
fromCardId: '1'
toCardId: '28'
status: valid
---
Le dérèglement climatique entraine une fonte des calottes glaciaires (= glaciers de très grande superficie). 
Les pôles sont les régions du monde où les impacts du changement climatique sont les plus visibles, les températures s'y réchauffent plus vite et la fonte des glaces s'y accélère. 
Un point de bascule entrainant la disparition complète du Groenland et de l'Antartique de l'Ouest n'est pas établi, mais pas impossible.
Il n'est pas question ici des glaciers, même si ils sont aussi impactés par de réchaufement climatique.
