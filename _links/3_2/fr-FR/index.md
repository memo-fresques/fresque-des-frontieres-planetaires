---
fromCardId: '3'
toCardId: '2'
status: invalid
---
Le CO2 n'est pas responsable de la destruction des molécules d'ozone O3, ce sont les molécules de chlore (qui sont contenues dans les gaz chlorés CFC).
