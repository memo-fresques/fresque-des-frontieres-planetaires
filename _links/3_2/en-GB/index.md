---
fromCardId: '3'
toCardId: '2'
status: invalid
---
CO2 is not responsible for the destruction of O3 ozone molecules, it is chlorine molecules (which are contained in CFC chlorine gases).
