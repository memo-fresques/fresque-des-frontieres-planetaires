---
fromCardId: '16'
toCardId: '23'
status: valid
---
Some of the agricultural fertilizers containing nitrogen and phosphorus do not remain in the fields and pollute rivers and oceans.
These nitrogen and phosphorus flows are at least twice the safe zone threshold: this process is in the significant risk zone.
