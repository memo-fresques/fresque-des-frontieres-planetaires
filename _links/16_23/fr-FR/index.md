---
fromCardId: '16'
toCardId: '23'
status: valid
---
Une partie des engrais agricoles contenant de l’azote et du phosphore ne reste pas dans les champs et pollue rivières et océans.
Ces flux d’azote et de phosphore sont au moins 2 fois supérieurs au seuil de la zone sûre : ce processus est dans la zone de risque important.
