---
fromCardId: '1'
toCardId: '19'
status: invalid
---
Ce n'est pas le réchauffement climatique qui entraine l'acidification des océans. Ces 2 phénomènes (changement climatique et acidification des océans) ont la même cause : les émissions de CO2. Une partie des émissions de CO2 (environ la moitié) reste dans l'atmosphère, ce qui augmente l'effet de serre et réchauffe le climat (changement climatique). Une partie des émissions de CO2 (environ 1/4) est absorbée par les océans. La dissolution du CO2 dans l'océan l'acidifie. Le reste des émissions de CO2 (environ 1/4) est absorbé par la végétation via la photosythèse.
