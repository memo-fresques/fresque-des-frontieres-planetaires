---
fromCardId: '1'
toCardId: '19'
status: invalid
---
It is not global warming that is causing ocean acidification. These two phenomena (climate change and ocean acidification) have the same cause: CO2 emissions. Part of CO2 emissions (around half) remains in the atmosphere, which increases the greenhouse effect and warms the climate (climate change). Part of CO2 emissions (around 1/4) is absorbed by the oceans. The dissolution of CO2 in the ocean acidifies it. The rest of CO2 emissions (around 1/4) are absorbed by vegetation via photosythesis.
