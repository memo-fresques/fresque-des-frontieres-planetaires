---
fromCardId: '22'
toCardId: '6'
status: optional
---
La déforestation (qui est la principale cause du changement d'affectation des sols) entraine des risques supplémentaires : érosion des sols, risque d’inondations et coulées d’eau boueuse.
