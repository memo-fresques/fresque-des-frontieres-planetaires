---
fromCardId: '22'
toCardId: '6'
status: optional
---
Deforestation (which is the main cause of land use change) causes additional risks: soil erosion, risk of flooding and muddy water flows.
