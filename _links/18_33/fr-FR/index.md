---
fromCardId: '18'
toCardId: '33'
status: valid
---
500 000 tonnes de microplastiques sont déversées chaque année dans les océans. Une majorité de ces microparticules proviennent du lavage des textiles synthétiques (60 % de la composition de nos vêtements).
Cette pollution, en forte hausse, affecte « au moins 267 espèces, dont 86 % des tortues marines, 44 % des oiseaux marins et 43 % des mammifères marins. »
