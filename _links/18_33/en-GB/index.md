---
fromCardId: '18'
toCardId: '33'
status: valid
---
500,000 tonnes of microplastics are dumped into the oceans each year. A majority of these microparticles come from the washing of synthetic textiles (60% of the composition of our clothes).
This pollution, which is increasing sharply, affects “at least 267 species, including 86% of sea turtles, 44% of seabirds and 43% of marine mammals. »
