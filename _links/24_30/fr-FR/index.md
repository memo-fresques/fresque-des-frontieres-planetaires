---
fromCardId: '24'
toCardId: '30'
status: valid
---
Le grand cycle de l'eau alimente entre autre les rivières et nappes phréatiques. Un cycle de l'eau perturbé et réparti géographiquement de manière différente a des impacts sur la disponibilité d'eau douce dans certaines régions.
