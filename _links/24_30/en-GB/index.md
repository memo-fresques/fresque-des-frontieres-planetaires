---
fromCardId: '24'
toCardId: '30'
status: valid
---
The great water cycle feeds, among other things, rivers and groundwater. A disrupted water cycle distributed differently geographically has impacts on the availability of fresh water in certain regions.
