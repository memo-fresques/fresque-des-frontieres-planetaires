---
fromCardId: '5'
toCardId: '3'
status: valid
---
The combustion of fossil fuels causes CO2 emissions into the atmosphere.
CO2 represents more than 70% of anthropogenic GHGs (greenhouse gases) globally.
