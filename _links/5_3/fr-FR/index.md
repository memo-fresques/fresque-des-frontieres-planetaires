---
fromCardId: '5'
toCardId: '3'
status: valid
---
La combustion des énergies fossiles entraine des émissions de CO2 dans l'atmosphère.
Le CO2 représente plus de 70% des GES (gaz à effet de serre) anthropiques au niveau mondial.
