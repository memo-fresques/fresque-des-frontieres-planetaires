---
fromCardId: '10'
toCardId: '1'
status: optional
---
CFCs (Chlorofluorocarbons) are GHGs, and therefore contribute to climate change. However, their overall impact is lower than that of CO2 / CH4 / N2O (although sometimes having a much higher GWP).
Their replacements (HFC - Hydrofluorocarbon, which no longer contain Chlorine and are therefore no longer destructive of Ozone) are even more powerful GHGs than CFCs...
This is why the Kigali Agreement in 2016 aims to reduce HFC emissions by 80-85% by 2036 to 2047 depending on the country. HFCs will gradually be replaced by hydrofluoroolefins (HFOs) with no impact on the ozone layer and much more limited greenhouse effects.
