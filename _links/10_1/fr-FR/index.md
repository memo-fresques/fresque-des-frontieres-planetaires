---
fromCardId: '10'
toCardId: '1'
status: optional
---
Les CFC (Chlorofluorocarbure) sont des GES, et donc contribuent au dérèglement climatique. Pour autant, leur impact global est inférieur à celui du CO2 / CH4 / N2O (bien qu'ayant un PRG parfois bien supérieur ).
Leurs remplaçants (HFC - Hydrofluorocarbure, qui ne contiennent plus de Chlore et ne sont donc plus destructeurs d'Ozone) sont des GES encore plus puissants que les CFC...
C'est pourquoi l’Accord de Kigali en 2016 a pour but de réduire les émissions de HFC de 80-85% d’ici 2036 à 2047 selon les pays. Les HFC seront progressivement remplacés par des hydrofluoroléfines (HFO) sans impact sur la couche d’ozone et aux effets de serre beaucoup + limités.
