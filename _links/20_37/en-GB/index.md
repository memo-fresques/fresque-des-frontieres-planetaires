---
fromCardId: '20'
toCardId: '37'
status: valid
---
“Fine particles (PM10, PM2.5, mainly from automobiles and wood heating) are found in large quantities in urban areas, so much so that most of the world's population is exposed to them.
Fine particles enter our lungs. Air pollution is responsible for 50 to 60,000 premature deaths in France, and 4.5 to 8.8 million worldwide.
91% of the world's population lives in places where WHO air quality guidelines are not met (2016 study)."
