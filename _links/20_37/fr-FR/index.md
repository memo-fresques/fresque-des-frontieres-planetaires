---
fromCardId: '20'
toCardId: '37'
status: valid
---
"Les particules fines (PM10, PM2,5, issus principalement de l'automobile et du chauffage au bois) se retrouvent en grande quantité dans les zones urbaines, si bien que l'essentiel de la population mondiale y est exposé.
Les particules fines pénètrent dans nos poumons. La pollution atmosphérique est responsable de 50 à 60 000 décès prématurés en France, et 4,5 à 8,8 millions dans le monde.
91% de la population mondiale vit dans des endroits où les lignes directrices de l’OMS relatives à la qualité de l’air ne sont pas respectées (étude de 2016)."
