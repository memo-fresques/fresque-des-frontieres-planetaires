---
fromCardId: '1'
toCardId: '6'
status: valid
---

Le déréglement climatique entraine l'augmentation de la fréquence et/ou de l'intensité des aléas climatiques : canicules, cyclones, sécheresses, crues, ...
