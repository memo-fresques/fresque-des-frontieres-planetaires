---
fromCardId: '1'
toCardId: '6'
status: valid
---


Climate change leads to an increase in the frequency and/or intensity of climatic hazards: heatwaves, cyclones, droughts, floods, etc.
