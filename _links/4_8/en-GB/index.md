---
fromCardId: '4'
toCardId: '8'
status: valid
---


Industry, transport, housing are human activities that meet needs (housing, transportation, communication, entertainment, etc.). These human activities require resources: energy (more than 80% of which is of fossil origin) and materials, many of which are metals and minerals.
