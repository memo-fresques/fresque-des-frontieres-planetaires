---
fromCardId: '4'
toCardId: '8'
status: valid
---

L'industrie, le transport, le logement sont des activités humaines qui répondent à des besoins (se loger, se déplacer, communiquer, se divertir, ...). Ces activités humaines nécessitent des ressources : énergie (dont plus de 80% est d'origine fossile) et matières dont beaucoup de métaux et minéraux.
