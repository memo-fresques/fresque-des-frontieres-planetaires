---
fromCardId: '29'
toCardId: '1'
status: valid
---
La savanisation de l’Amazonie accentuerait en retour le réchauffement climatique par le relargage dans l’atmosphère du carbone contenu dans la végétation.
