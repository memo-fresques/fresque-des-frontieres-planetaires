---
fromCardId: '33'
toCardId: '39'
status: valid
---


“A decline in pollinating insects can impact agricultural yields (75% of global food production depends on pollinating insects).
A decline in corals and plankton can impact fishery resources (fish, crustaceans, molluscs).
In Asia, more than a billion people depend on fishing as their primary food resource (animal protein), particularly in low-income countries.
