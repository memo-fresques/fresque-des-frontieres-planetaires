---
fromCardId: '33'
toCardId: '39'
status: valid
---

"Une baisse des insectes polinisateurs peut impacter les rendement agricoles (75% de la production mondiale de nourriture dépend des insectes pollinisateurs).
Une baisse des coraux et planctons peut impacter les ressources halieutiques (poissons, crustacés, mollusques).
En Asie, plus d’un milliard de personnes dépendent de la pêche comme première ressource alimentaire (en protéines animales), en particulier dans les pays à faibles revenus."
