---
fromCardId: '14'
toCardId: '13'
status: valid
---
Agriculture requires freeing up areas dedicated to direct human food (or indirectly via animal feed and pastures). This release of surface area leads to the destruction of natural habitats, particularly through deforestation. The vast majority of deforestation is caused by agriculture.
Soy cultivation in Brazil (mainly to feed livestock) contributes significantly to deforestation in the Cerrado and Amazon.
