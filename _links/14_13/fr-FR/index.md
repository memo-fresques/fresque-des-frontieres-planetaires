---
fromCardId: '14'
toCardId: '13'
status: valid
---
L'agriculture nécessite de libérer des surfaces dédiées à l'alimentation humaine directe (ou indirecte via l'alimentation animale et les paturages). Cette libération de surface conduit à détruire des habitats naturels notamment au travers de la déforestation. La déforestation est en très grande majorité le fait de l’agriculture.
La culture du soja au Brésil (principalement pour nourrir du bétail) contribue fortement à la déforestation du Cerrado et de l'Amazonie.
