---
fromCardId: '3'
toCardId: '1'
status: valid
---
Le CO2 est le principal gaz à effet de serre d'origine anthropique. Une partie est absorbé dans les puits de carbone naturels (océans et végétation via la photosynthèse). Le reste augmente la concentration dans l'atmosphère et accentue l'effet de serre naturel. 
