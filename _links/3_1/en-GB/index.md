---
fromCardId: '3'
toCardId: '1'
status: valid
---
CO2 is the main greenhouse gas of anthropogenic origin. Part of it is absorbed into natural carbon sinks (oceans and vegetation via photosynthesis). The rest increases the concentration in the atmosphere and accentuates the natural greenhouse effect.
