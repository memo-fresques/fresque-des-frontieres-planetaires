---
fromCardId: '31'
toCardId: '39'
status: valid
---
“We must consider here the yields of fishing.
25% of marine biodiversity depends on coral reefs. Fewer corals means fewer fisheries resources.”
