---
fromCardId: '14'
toCardId: '5'
status: optional
---
L'agriculture utilise très peu d'énergie fossile, au regard des émissions d'autres GES dont elle est responsable. Pour autant, le lien n'est pas faux.
