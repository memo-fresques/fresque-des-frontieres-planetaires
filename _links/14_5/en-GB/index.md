---
fromCardId: '14'
toCardId: '5'
status: optional
---
Agriculture uses very little fossil energy, compared to the emissions of other GHGs for which it is responsible. However, the link is not false.
