---
fromCardId: '19'
toCardId: '31'
status: optional
---
Acidification alters the calcification processes which limit the development of corals, shelled animals and plankton which are at the base of the marine food chain. All marine biodiversity is thus threatened. For corals, the main cause of disappearance is not acidification but marine heat waves (climate change)
