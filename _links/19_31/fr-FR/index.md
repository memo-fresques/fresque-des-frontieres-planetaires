---
fromCardId: '19'
toCardId: '31'
status: optional
---
L’acidification altère les processus de calcification qui limitent le développement des coraux, des animaux à coquilles et du plancton qui est à la base de la chaîne alimentaire marine. Toute la biodiversité marine s’en trouve ainsi menacée. Pour les coraux, la première cause de disparition n'est pas l'acidification mais les vagues de chaleur marines (changement climatique)
