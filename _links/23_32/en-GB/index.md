---
fromCardId: '23'
toCardId: '32'
status: valid
---
The increase in the concentration of nutrients in an aquatic environment leads to a phenomenon called eutrophication. This is particularly the case with nitrogen and phosphorus, particularly following the use of nitrogen and phosphate fertilizers.
