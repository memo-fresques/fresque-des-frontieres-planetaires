---
fromCardId: '23'
toCardId: '32'
status: valid
---
L'augmentation de la concentration en nutriments dans un milieu milieu aquatique entraine un phénomène appelé eutrophisation. C'est le cas notamment avec l'azote et le phosphore, notamment suite à l'utilisation des engrais azotés et phosphatés.
