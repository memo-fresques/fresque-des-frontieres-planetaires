---
fromCardId: '8'
toCardId: '10'
status: valid
---
Chlorinated gases are, for example, CFCs used in cooling systems (refrigerators, air conditioners, etc.), or as propellant gas in aerosol cans, not to be confused with "aerosols" (technical term) which are fine particles in suspended in the air. Chlorinated gases have been replaced by the use of fluorinated gases, which do not destroy the ozone layer.
