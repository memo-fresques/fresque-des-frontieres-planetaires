---
fromCardId: '8'
toCardId: '10'
status: valid
---
Les gaz chlorés sont par exemples les CFC utilisés dans les systèmes de refroidissements (réfrigérateurs, climatiseurs…), ou comme gaz propulseur dans les bombes aérosols, à ne pas confondre avec les "aérosols" (terme technique) qui sont eux des particules fines en suspension dans l'air. Les gaz chlorés ont été remplacés par l'utilisation de gaz fluorés, qui ne détruisent pas la couche d'ozone.
