---
fromCardId: '20'
toCardId: '26'
status: valid
---
For water vapor to condense, it needs a “core”. Aerosols can constitute this core, promoting the condensation of water vapor and the formation of raindrops.
