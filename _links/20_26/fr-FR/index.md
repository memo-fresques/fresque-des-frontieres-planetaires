---
fromCardId: '20'
toCardId: '26'
status: valid
---
Pour que la vapeur d'eau condense, il lui faut un "noyau". Les aérosols peuvent constituer ce noyau, favorisant la condensation de la vapeur d'eau, et la formation de gouttes de pluie. 
