---
fromCardId: '1'
toCardId: '31'
status: valid
---
Le corail est une symbiose entre des colonies de polypes et des algues photosynthétiques, les zooxanthelles, qui donnent sa belle couleur au corail.

Si la température de l'eau augmente (du fait du déréglement climatique), le corail expulse ses micro-algues (la symbiose est cassée). Dépigmenté, il laisse voir son squelette blanc.

Si cela se prolonge, le corail meurt, et des algues filamenteuses recouvrent son squelette.

A +1,5°C, 70 à 90 % des coraux disparaissent
A +2°C, plus de 99 % des coraux disparaissent.
