---
fromCardId: '1'
toCardId: '31'
status: valid
---
Coral is a symbiosis between colonies of polyps and photosynthetic algae, zooxanthellae, which give coral its beautiful color.


If the water temperature increases (due to climate change), the coral expels its micro-algae (the symbiosis is broken). Depigmented, it reveals its white skeleton.


If this continues, the coral dies, and filamentous algae covers its skeleton.


At +1.5°C, 70 to 90% of corals disappear
At +2°C, more than 99% of corals disappear.
