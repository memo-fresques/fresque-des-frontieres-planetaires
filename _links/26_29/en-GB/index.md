---
fromCardId: '26'
toCardId: '29'
status: valid
---
In response to global warming, precipitation is expected to decrease in the Amazon and the dry season to lengthen. This drying contributes to the savannahization of the Amazon (in addition to deforestation).
