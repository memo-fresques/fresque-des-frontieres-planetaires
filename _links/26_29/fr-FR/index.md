---
fromCardId: '26'
toCardId: '29'
status: valid
---
En réponse au réchauffement climatique, il est attendu que les précipitations diminuent en Amazonie et que la saison sèche s'allonge. Cet assèchement participe à la savanisation de l'Amazonie (en plus de la déforestation).
