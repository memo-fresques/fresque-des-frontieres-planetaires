---
fromCardId: '6'
toCardId: '31'
status: optional
---
Ce sont principalement les vagues de chaleur marine qui tuent les coraux. La carte "disparition des coraux" peut donc être reliée en amont par les participant.e.s à la carte "changement climatique" ou "augmentation des aléas climatiques", dont les vagues de chaleur marine font partie.
