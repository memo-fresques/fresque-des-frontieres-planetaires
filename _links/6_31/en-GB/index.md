---
fromCardId: '6'
toCardId: '31'
status: optional
---
It is mainly marine heat waves that kill corals. The “disappearance of corals” map can therefore be linked upstream by the participants to the “climate change” or “increase in climatic hazards” map, of which marine heat waves are a part.
