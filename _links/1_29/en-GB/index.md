---
fromCardId: '1'
toCardId: '29'
status: valid
---
The combination of deforestation, drier conditions and increased fires could push the rainforest ecosystem past a tipping point.
