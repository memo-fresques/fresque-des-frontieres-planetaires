---
fromCardId: '1'
toCardId: '29'
status: valid
---
La combinaison de la déforestation, des conditions plus sèches et de l'augmentation des incendies pourrait faire franchir à l'écosystème de la forêt tropicale un point de bascule.
