---
fromCardId: '2'
toCardId: '27'
status: valid
---

La distinction UVA/UVB/UVC tient à une plage de longueur d'ondes différente. Les UVC n'atteignent jamais la surface terrestre, ils sont entièrement filtrés par l'atmosphère. Les UVB et UVA traversent en partie l'atmosphère en fonction de la concentration en azote, oxygène, dioxygène et ozone. Ce dernier est le plus influant sur la filtration des UVB.
