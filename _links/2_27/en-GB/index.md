---
fromCardId: '2'
toCardId: '27'
status: valid
---


The UVA/UVB/UVC distinction is due to a different wavelength range. UVC never reaches the earth's surface, it is entirely filtered by the atmosphere. UVB and UVA partially pass through the atmosphere depending on the concentration of nitrogen, oxygen, oxygen and ozone. The latter is the most influential on UVB filtration.
