---
fromCardId: '5'
toCardId: '9'
status: valid
---
The manufacture of “conventional” plastic is entirely based on the exploitation of oil. At the start, plastic was “only” a “by-product”.
