---
fromCardId: '5'
toCardId: '9'
status: valid
---
La fabrication du plastique "conventionnel" repose entièrement sur l'exploitation du pétrole. Au démarrage, le plastique était "seulement" un "sous-produit".
