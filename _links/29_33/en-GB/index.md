---
fromCardId: '29'
toCardId: '33'
status: optional
---
The ecosystem services at stake here include the storage of CO2 in trees, the loss of biodiversity, climate regulation, pharmacopoeia.
