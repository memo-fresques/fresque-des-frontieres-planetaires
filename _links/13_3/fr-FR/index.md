---
fromCardId: '13'
toCardId: '3'
status: valid
---
La déforestation (première causes de destruction des habitats) consiste dans la plupart des cas (93%) à bruler les arbres sur place, ce qui relargue très rapidement le carbone qui avait été stocké lors de la croissance des arbres. Cela représente une contribution de 15% des émissions mondiales de CO2.
