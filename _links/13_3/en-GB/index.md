---
fromCardId: '13'
toCardId: '3'
status: valid
---
Deforestation (primary cause of habitat destruction) consists in most cases (93%) of burning trees on site, which very quickly releases the carbon which had been stored during the growth of the trees. This represents a contribution of 15% of global CO2 emissions.
