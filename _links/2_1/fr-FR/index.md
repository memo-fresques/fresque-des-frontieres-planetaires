---
fromCardId: '2'
toCardId: '1'
status: optional
---

Le lien couche d'ozone -> changement climatique peut être vrai ou faux selon la justification donnée. La couche d'ozone filtre une partie des UV, donc contribue à limiter l'énergie qui arrive à la surface de la terre. Cependant, cet impact n'est pas prépondérant. Ce lien est donc vrai, même si secondaire. Il est probable que la plupart des participant·e·s faisant ce lien le fassent avec une mauvaise justification en tête (confusion des phénomènes couche d'ozone et changement climatique). Il est donc important de vérifier ce qu'ils et elles ont en tête avec ce lien.
Lors de pics de pollution, il y a production d'ozone à basse altitude, ce qui est un gaz à effet de serre et réchauffe localement. En revanche cet ozone étant produit à basse altitude, il ne fait pas partie de la "couche d'ozone", et ne permet donc pas de justifier un lien couche d'ozone -> changement climatique
