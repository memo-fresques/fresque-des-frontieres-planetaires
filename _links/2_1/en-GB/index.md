---
fromCardId: '2'
toCardId: '1'
status: optional
---


The ozone layer -> climate change link can be true or false depending on the justification given. The ozone layer filters part of UV rays, therefore helping to limit the energy reaching the earth's surface. However, this impact is not significant. This link is therefore true, even if secondary. It is likely that most of the participants making this link do so with a poor justification in mind (confusion of the ozone layer and climate change phenomena). It is therefore important to check what they have in mind with this link.
During pollution peaks, ozone is produced at low altitude, which is a greenhouse gas and heats up locally. On the other hand, this ozone being produced at low altitude, it is not part of the "ozone layer", and therefore does not justify an ozone layer -> climate change link.
