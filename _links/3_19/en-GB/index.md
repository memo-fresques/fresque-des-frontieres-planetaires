---
fromCardId: '3'
toCardId: '19'
status: valid
---


The oceans absorbed around 1/4 of CO2 emissions. Dissolving CO2 in water acidifies it.
