---
fromCardId: '3'
toCardId: '19'
status: valid
---

Les océans ont absorbé environ 1/4 des émissions de CO2. La dissolution du CO2 dans l'eau l'acidifie.
