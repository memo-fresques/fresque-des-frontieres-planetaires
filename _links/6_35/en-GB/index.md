---
fromCardId: '6'
toCardId: '35'
status: optional
---
Some areas can become uninhabitable after extreme events, leading to population displacement.
