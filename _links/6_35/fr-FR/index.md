---
fromCardId: '6'
toCardId: '35'
status: optional
---
Certaines zones peuvent devenir inhabitables après des évènements extrêmes, entrainant le déplacement de population.
