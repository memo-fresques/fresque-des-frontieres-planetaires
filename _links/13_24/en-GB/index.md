---
fromCardId: '13'
toCardId: '24'
status: valid
---
Trees have an important role in regulating humidity and storing water in soils.
