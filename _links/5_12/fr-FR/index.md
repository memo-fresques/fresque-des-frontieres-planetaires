---
fromCardId: '5'
status: valid
toCardId: '12'
---

Le méthane représente 17% des GES au niveau mondial (principalement agriculture, mais aussi lors de la production (fuites notamment) et combustion d'énergies fossiles). Le protoxyde d'azote n'est pas directement liés à l'utilisation des énergies fossiles.
