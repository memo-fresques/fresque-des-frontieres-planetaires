---
fromCardId: '5'
status: valid
toCardId: '12'
---


Methane represents 17% of GHGs globally (mainly agriculture, but also during production (particularly leaks) and combustion of fossil fuels). Nitrous oxide is not directly linked to the use of fossil fuels.
