---
fromCardId: '6'
toCardId: '39'
status: valid
---
Les aléas climatiques peuvent dégrader les rendements agricoles : destruction de cultures par des tempêtes, des cyclones, de la grêle, des inondations, des canicules, des sécheresses, ...
