---
fromCardId: '6'
toCardId: '39'
status: valid
---
Climatic hazards can degrade agricultural yields: destruction of crops by storms, cyclones, hail, floods, heatwaves, droughts, etc.
