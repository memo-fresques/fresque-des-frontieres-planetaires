---
fromCardId: '16'
toCardId: '12'
status: valid
---
L'épandage des engrais azotés est responsable de la plupart des émissions de protoxyde d'azote (N2O). 
La fabrication des engrais azotés utilisent du méthane pour être fabriqués, mais il n'y pas d'émissions de méthane lors de l'utilisation des engrais.
