---
fromCardId: '16'
toCardId: '12'
status: valid
---
The application of nitrogen fertilizers is responsible for most nitrous oxide (N2O) emissions.
Nitrogen fertilizers use methane to make them, but there are no methane emissions when using the fertilizers.
