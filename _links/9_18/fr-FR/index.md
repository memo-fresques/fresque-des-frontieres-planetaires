---
fromCardId: '9'
toCardId: '18'
status: valid
---
Il pleut du plastique. Une étude effectuée dans les Pyrénées montre le dépôt de 365 micro-particules de plastique par m2 par jour (publiée dans Nature en 2019)

8 millions de tonnes : c’est la quantité de plastique polluant les écosystèmes marins chaque année selon la Fondation Ellen MacArthur. C’est l’équivalent d’un camion poubelle déchargé en mer chaque minute. Si rien n’est mis en œuvre, ce nombre passera à deux par minute d’ici à 2030, et à quatre par minute d’ici à 2050, précise la Fondation
