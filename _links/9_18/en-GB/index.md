---
fromCardId: '9'
toCardId: '18'
status: valid
---
It's raining plastic. A study carried out in the Pyrenees shows the deposition of 365 plastic micro-particles per m2 per day (published in Nature in 2019)


8 million tonnes: this is the quantity of plastic polluting marine ecosystems each year according to the Ellen MacArthur Foundation. This is the equivalent of one garbage truck being dumped at sea every minute. If nothing is implemented, this number will increase to two per minute by 2030, and to four per minute by 2050, specifies the Foundation
