---
fromCardId: '25'
toCardId: '39'
status: valid
---
The decline of plankton impacts the entire marine food chain and therefore fish resources.
