---
fromCardId: '39'
toCardId: '36'
status: valid
---
La baisse des rendements agricoles et l’effondrement de la biodiversité marine peuvent engendrer des famines.
