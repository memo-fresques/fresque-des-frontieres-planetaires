---
fromCardId: '39'
toCardId: '36'
status: valid
---
Declining agricultural yields and the collapse of marine biodiversity can lead to famine.
