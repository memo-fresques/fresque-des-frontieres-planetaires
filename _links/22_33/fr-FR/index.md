---
fromCardId: '22'
toCardId: '33'
status: optional
---
Moins de forets, c'est moins de régulation du taux de CO2 dans l'atmosphère (moins de stockage de carbone, et moins de production d'O2 en retour).
