---
fromCardId: '22'
toCardId: '33'
status: optional
---
Fewer forests mean less regulation of the CO2 level in the atmosphere (less carbon storage, and less O2 production in return).
