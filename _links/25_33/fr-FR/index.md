---
fromCardId: '25'
toCardId: '33'
status: optional
---
Les planctons sont à la base de la chaine alimentaire marine. Moins de planctons, c'est moins de poissons (ou des moins gros).
