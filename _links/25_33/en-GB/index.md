---
fromCardId: '25'
toCardId: '33'
status: optional
---
Plankton are at the base of the marine food chain. Fewer plankton means fewer (or smaller) fish.
