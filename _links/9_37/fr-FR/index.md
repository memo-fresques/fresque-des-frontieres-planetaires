---
fromCardId: '9'
toCardId: '37'
status: optional
---
D’après étude de l’Université de Newcastle, un être humain pourrait ingérer environ 5 grammes de plastique chaque semaine soit l’équivalent de la quantité de microplastiques contenue dans une carte de crédit.
