---
fromCardId: '9'
toCardId: '37'
status: optional
---
According to a study by the University of Newcastle, a human being could ingest around 5 grams of plastic each week, the equivalent of the quantity of microplastics contained in a credit card.
