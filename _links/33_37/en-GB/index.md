---
fromCardId: '33'
toCardId: '37'
status: valid
---
Many medicines are extracted from living organisms. A reduction in this ecosystem service could have impacts on the treatment of certain diseases.
