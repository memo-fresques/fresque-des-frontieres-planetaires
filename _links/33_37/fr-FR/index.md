---
fromCardId: '33'
toCardId: '37'
status: valid
---
Une bonne partie des médicaments sont extraits d'organismes vivants. Une diminution de ce service écosystémique pourrait avoir des impacts sur le traitement de certaines maladies.
