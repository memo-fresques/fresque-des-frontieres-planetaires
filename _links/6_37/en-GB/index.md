---
fromCardId: '6'
toCardId: '37'
status: optional
---
Following floods and cyclones, disaster areas can be exposed to health problems (access to drinking water, spread of diseases).
