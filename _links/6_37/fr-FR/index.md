---
fromCardId: '6'
toCardId: '37'
status: optional
---
Suite à des crues, des cyclones, les zones sinistrées peuvent être exposées à des problèmes sanitaires (accès à l'eau potable, propagation de maladies).
