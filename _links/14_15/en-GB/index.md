---
fromCardId: '14'
toCardId: '15'
status: valid
---
• Productivist agriculture/breeding accounts for 70% of drinking water withdrawals in the world (and up to 95% in certain developing countries), industry (20%) and domestic consumption 10%.
• The reason is the gigantic irrigation of our crops. Some examples: 1kg of beef = 13,500 liters of water. 1 kg of cotton = 5,263 liters. 1 liter of milk = 1000 liters. 1 piece of white sugar = 10 liters. 1 liter of bottled water = 7 liters
• In addition, a large part of irrigation water is lost through leakage and evaporation.
• Among the biggest consumers: Nestlé (800 million liters per year) and Coca-Cola. The manufacture of 1 liter of Coke requires 2.5 to 6 liters of water, which is equivalent, for the global consumption of 350 billion liters of Coke/year, to 2100 billion liters of water/year for this alone. beverage ! To meet its enormous needs, the firm empties the aquifer resources of Mexico, India, Indonesia, Malaysia and African countries.
