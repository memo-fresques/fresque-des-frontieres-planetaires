---
fromCardId: '14'
toCardId: '15'
status: valid
---
• L’agriculture/élevage productiviste réalise 70% des prélèvements d’eau potable dans le monde (et jusqu’à 95% dans certains pays en développement), l’industrie (20%) et la consommation domestique 10%.
• La raison est l’irrigation gigantesque de nos cultures. Quelques exemples : 1kg de viande de bœuf = 13 500 litres d’eau. 1 kg de coton = 5 263 litres. 1 litre de lait = 1000 litres. 1 morceau de sucre blanc = 10 litres. 1 litre d’eau en bouteille = 7 litres
• En outre, une grande partie de l’eau d’irrigation est perdue par fuites et évaporation.
• Parmi les industriels les plus consommateurs : Nestlé (800 millions de litres par an) et Coca-Cola. La fabrication d’1 litre de Coca demande 2,5 à 6 litres d’eau, ce qui équivaut, pour la consommation mondiale de 350 milliards de litres de Coca / an, à  2100 milliards de litres d’eau / an pour cette seule boisson ! Pour satisfaire ses besoins énormes, la firme vide les ressources aquifères du Mexique, d’Inde, Indonésie, Malaisie et de pays d’Afrique
