---
fromCardId: '16'
toCardId: '21'
status: optional
---
The use of fertilizers and pesticides are the main causes of the disappearance of birds in Europe according to the article [Farmland practices are driving bird population decline across Europe](https://www.pnas.org/doi/10.1073/ pnas.2216573120)
