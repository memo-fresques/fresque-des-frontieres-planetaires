---
fromCardId: '16'
toCardId: '21'
status: optional
---
L’utilisation d’engrais et de pesticides sont les principaux responsables de la disparition des oiseaux en Europe selon l’article [Farmland practices are driving bird population decline across Europe](https://www.pnas.org/doi/10.1073/pnas.2216573120)
