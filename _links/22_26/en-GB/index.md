---
fromCardId: '22'
toCardId: '26'
status: valid
---
“A change in land use through deforestation impacts the regulatory role of trees on humidity. Which impacts the rain regime locally. Even globally for tropical and boreal forests.
A large oak tree transpires up to 1600 liters of water per day. European forests sweat about 400 mm per year, about half of the continent's precipitation."
