---
fromCardId: '22'
toCardId: '26'
status: valid
---
"Un changement d'affectation des sols par la déforestation impacte le role régulateurs des arbres sur l'humidité. Ce qui impacte localement le régime des pluies. Voire mondialement pour ce qui est des forets tropicales et boréales.
Un grand chêne transpire jusqu’à 1600 litres d’eau par jour. Les forêts européennes transpirent environ 400 mm par an, soit environ la moitié des précipitations sur le continent."
