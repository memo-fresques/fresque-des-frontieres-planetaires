---
fromCardId: '18'
toCardId: '37'
status: valid
---
Once in the environment as macro- or microplastic, plastic contaminates and accumulates in agricultural soils, terrestrial and aquatic food chains, and water supplies. Microplastics that enter the human body through direct exposure, either by ingestion or inhalation, can cause various health problems; inflammation,
genotoxicity, oxidative stress, apoptosis, and necrosis. These mechanisms are linked to many health problems, including
cancers, cardiovascular diseases, intestinal inflammation, diabetes, rheumatoid arthritis, chronic inflammation, autoimmune diseases, neurodegenerative diseases, and strokes
https://www.ciel.org/wp-content/uploads/2019/03/Plastic-Health-French.pdf
