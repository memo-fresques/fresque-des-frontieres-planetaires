---
fromCardId: '18'
toCardId: '37'
status: valid
---
Une fois dans l’environnement sous forme de macro- ou de microplastique, le plastique contamine et s’accumule dans les sols agricoles, les chaînes alimentaires terrestres et aquatiques, et les sources d’approvisionnement en eau. Les microplastiques qui pénètrent le corps humain par voie d’exposition directe, soit par ingestion ou inhalation, peuvent causer des troubles de santé divers; inflammations,
génotoxicité, stress oxydatif, apoptose, et nécrose. Ces mécanismes sont liés à de nombreux problèmes de santé, y compris
des cancers, maladies cardiovasculaires, inflammation de l’intestin, diabète, polyarthrite rhumatoïde, inflammation chronique, maladies auto-immunes, maladies neurodégénératives, et accidents vasculaires cérébraux
https://www.ciel.org/wp-content/uploads/2019/03/Plastic-Health-French.pdf
