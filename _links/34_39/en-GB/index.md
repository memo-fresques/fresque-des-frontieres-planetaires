---
fromCardId: '34'
toCardId: '39'
status: valid
---
Anoxic/hypoxic zones have a direct impact on fish populations, and can locally reduce fishery yields.
