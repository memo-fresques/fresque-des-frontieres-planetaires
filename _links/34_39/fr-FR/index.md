---
fromCardId: '34'
toCardId: '39'
status: valid
---
Les zones anoxiques / hypoxiques ont un impact direct sur les populations de poissons, et peuvent faire chuter localement les rendements de la pècherie.
