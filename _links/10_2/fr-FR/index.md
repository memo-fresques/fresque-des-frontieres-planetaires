---
fromCardId: '10'
toCardId: '2'
status: valid
---
L'ozone stratosphérique (qui constitue la couche d'ozone dans les hautes couches de l'atmosphère entre 20 et 40 km d'altitude), représente 90% de l'ozone présent dans l'atmosphère, et filtre 97% des rayons UVC. Il se forme par combinaison d'une molécule de dioxygène (O2) avec un atome isolé d'oxygène (c’est le rayonnement solaire qui crée ces atomes isolés en brisant les molécules d’oxygène en 2). Les gaz chlorés (comme les CFC, utilisés notamment comme gaz propulseur dans les bombes aérosols) qui arrivent dans la stratosphère sont décomposés par les UV et libèrent des atomes de chlore qui vont réagir avec l’ozone en le transformant en dioxygène.

Des trous dans la couche d'ozone ont été dans un premier temps limité à l'Antarctique, pour être ensuite observé à une moindre échelle sur le pôle Nord. Aujourd'hui, la Nouvelle Zélande et le Sud de l'Australie, mais également le Canada, la Scandinavie connaissent régulièrement de fortes baisses d'ozone stratosphérique au printemps.
Les conditions météorologiques sont différentes entre le pole sud et le pole nord, ce qui pourrait expliquer pourquoi le phénomène est plus marqué en Antarctique (sud) qu'en arctique (nord). Notamment parce qu'il y a plus de renouvellement des masses d'air au pole nord, alors que dans l'hémisphère sud, des courants aériens "isolent" les masses d'air, s'en suit un brassage moins important, et donc pas d'apport d'ozone depuis les régions équatoriales.

Les gouvernements ont décidé de mettre fin à la production des CFC dans le cadre du Protocole de Montréal qui est entré en vigueur le 1er janvier 1989. Ils sont remplacés dans un premier temps par des HCFC (qui sont moins destructeurs d'ozone), puis par des HFC dans un 2è temps (ils ne contiennent pas de chlore, donc ne vont pas réagir avec l'ozone). 
