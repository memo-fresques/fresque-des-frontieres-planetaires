---
fromCardId: '10'
toCardId: '2'
status: valid
---
Stratospheric ozone (which constitutes the ozone layer in the upper layers of the atmosphere between 20 and 40 km altitude), represents 90% of the ozone present in the atmosphere, and filters 97% of UVC rays . It is formed by the combination of a molecule of dioxygen (O2) with an isolated atom of oxygen (it is solar radiation which creates these isolated atoms by breaking the oxygen molecules in two). Chlorinated gases (like CFCs, used in particular as propellant gas in aerosol bombs) which reach the stratosphere are broken down by UV rays and release chlorine atoms which will react with ozone, transforming it into dioxygen.


Holes in the ozone layer were initially limited to Antarctica, before being observed on a smaller scale at the North Pole. Today, New Zealand and southern Australia, but also Canada and Scandinavia, regularly experience strong drops in stratospheric ozone in spring.
Meteorological conditions are different between the South Pole and the North Pole, which could explain why the phenomenon is more marked in Antarctica (south) than in the Arctic (north). Particularly because there is more renewal of air masses at the north pole, while in the southern hemisphere, air currents "isolate" the air masses, resulting in less mixing, and therefore no ozone contribution from equatorial regions.


Governments decided to end the production of CFCs within the framework of the Montreal Protocol which came into force on January 1, 1989. They were replaced initially by HCFCs (which are less destructive of ozone), then by HFCs in a second step (they do not contain chlorine, therefore will not react with ozone).
