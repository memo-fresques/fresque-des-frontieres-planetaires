---
fromCardId: '34'
toCardId: '33'
status: optional
---


In January 2018, the international working group “Global ocean oxygen network” indicated in the journal Science that the proportion of high seas areas devoid of all oxygen has more than quadrupled in 50 years (150 to 450 dead zones identified). Low oxygenated sites located near the coasts (including estuaries) have increased tenfold since 1950. These areas have increasingly significant impacts on fishing and ecosystems. Fish can escape from these areas (but rapid loss of consciousness possible). On the other hand, molluscs and crustaceans have too low a movement speed and die quickly.
