---
fromCardId: '34'
toCardId: '33'
status: optional
---

En janvier 2018, le groupe de travail international « Global ocean oxygen network » indique dans la revue Science que la proportion de zones de haute mer dépourvues de tout oxygène a plus que quadruplé en 50 ans (150 à 450 zones mortes recencées). Les sites faiblement oxygénés situés près des côtes (incluant les estuaires) ont été décuplés depuis 1950. Ces zones ont des impacts de plus en plus importants sur la pêche et les écosystèmes. Les poissons peuvent arriver à s'échapper de ces zones( mais perte de connaissance rapide possible). Par contre, les mollusques et crustacés ont une vitesse de déplacement trop faible, et meurent rapidement.
