---
fromCardId: '27'
toCardId: '33'
status: valid
---
Les rayons ultraviolets perturbent les divisions cellulaires des micro-organismes aquatiques, ce qui a de graves conséquences sur la vie aux pôles.
