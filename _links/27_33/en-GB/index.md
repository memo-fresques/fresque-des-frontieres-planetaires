---
fromCardId: '27'
toCardId: '33'
status: valid
---
Ultraviolet rays disrupt the cell divisions of aquatic microorganisms, which has serious consequences for life at the poles.
