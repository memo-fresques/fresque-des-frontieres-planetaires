---
fromCardId: '30'
toCardId: '39'
status: valid
---


Fresh water is essential for agriculture, which uses it in large quantities (70% of global consumption of blue water), whether it is blue water or green water.
