---
fromCardId: '30'
toCardId: '39'
status: valid
---

L'eau douce est indispensable à l'agriculture, qui l'utilise en grande quantité (70% de la consommation mondiale de l'eau bleue), que ce soit l'eau bleue ou l'eau verte.
