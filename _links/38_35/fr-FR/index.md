---
fromCardId: '38'
toCardId: '35'
status: valid
---
En cas de conflit, une partie des populations locales peut fuir les zones de combat et migrer pour retrouver la sécurité.
