---
fromCardId: '38'
toCardId: '35'
status: valid
---
In the event of conflict, some local populations may flee combat zones and migrate to find safety.
