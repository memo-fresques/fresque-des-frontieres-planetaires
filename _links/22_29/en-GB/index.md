---
fromCardId: '22'
toCardId: '29'
status: valid
---
“Deforestation is the other cause that could cause its transformation into savannah. 17% of the Amazon is currently deforested. Savanization could be triggered from 40% deforestation.


Deforestation in the Amazon basin accelerated greatly between 1991 and 2004, reaching an annual rate of forest area loss of 27,423 km2 in 2004. Although the rate of deforestation has slowed since 2004 (with a reacceleration in 2008 and in 2013), the area covered by forest continues to decrease.
The causes of this deforestation are cattle breeding, soya cultivation (animal feed and biodiesel), subsistence agriculture, and some infrastructure projects.


The Amazon rainforest alone represents half of the remaining tropical forests on Earth, and is the largest and most biodiverse tropical forest in the world.


