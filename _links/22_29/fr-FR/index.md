---
fromCardId: '22'
toCardId: '29'
status: valid
---
"La déforestation est l'autre cause qui pourrait provoquer sa transformation en savane. 17% de l’Amazonie est actuellement déboisée. La savanisation pourrait se déclencher à partir de 40% de déforestation.

La déforestation du bassin amazonien s'est grandement accélérée entre 1991 et 2004, jusqu'à atteindre un taux annuel de perte de surface forestière de 27 423 km2 en 2004. Bien que le taux de déforestation ait ralenti depuis 2004 (avec une réaccélération en 2008 et en 2013), la surface couverte par la forêt continue de décroître.
Les causes de cette déforestation sont l'élevage bovin, la culture du soja (alimentation animale et biodiesel), l'agriculture vivrière, et quelques projets d'infrastructures.

La forêt amazonienne représente à elle seule la moitié des forêts tropicales restantes sur la Terre, et est la forêt tropicale la plus grande, et celle avec la plus grande biodiversité au monde."

