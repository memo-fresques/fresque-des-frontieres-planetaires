---
fromCardId: '38'
toCardId: '37'
status: valid
---
Les conflits armés dégradent les infrastructures et réduisent l'accès aux biens de premières nécessité (nourriture, eau, ...)
