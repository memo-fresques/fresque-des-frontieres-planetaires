---
fromCardId: '38'
toCardId: '37'
status: valid
---
Armed conflicts degrade infrastructure and reduce access to basic necessities (food, water, etc.)
