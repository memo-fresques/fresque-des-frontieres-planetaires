---
fromCardId: '30'
toCardId: '37'
status: valid
---
Insufficient access to fresh water has direct effects on human health.
