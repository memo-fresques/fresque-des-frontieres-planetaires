---
fromCardId: '30'
toCardId: '37'
status: valid
---
Un accès à l'eau douce insuffisant a des effets directs sur la santé humaine.
