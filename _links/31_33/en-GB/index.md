---
fromCardId: '31'
toCardId: '33'
status: optional
---
25% of marine biodiversity depends on coral reefs. Fewer corals mean fewer fisheries resources.
