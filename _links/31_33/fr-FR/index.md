---
fromCardId: '31'
toCardId: '33'
status: optional
---
25% de la biodiversité marine dépend des récifs coralliens. Moins de coraux, c'est moins de ressource halieutique.
