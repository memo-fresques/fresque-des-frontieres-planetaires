---
fromCardId: '11'
toCardId: '2'
status: invalid
---
Aerosols are small particles suspended in the air (fine particles, soot from volcanoes, etc.) and have no impact on the ozone layer. They should not be confused with aerosol bombs which used chlorine gases (CFCs), which were responsible for the hole in the ozone layer.
