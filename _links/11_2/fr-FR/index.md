---
fromCardId: '11'
toCardId: '2'
status: invalid
---
Les aérosols sont des petites particules en suspension dans l'air (particules fines, suies des volcans, ...) et n'ont pas d'impact sur la couche d'ozone. Il ne faut pas les confondre avec les bombes aérosols qui elles, utilisaient des gaz chlorés (CFC), qui étaient responsables du trou dans la couche d'ozone.
