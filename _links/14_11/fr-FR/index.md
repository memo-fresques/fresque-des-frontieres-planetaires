---
fromCardId: '14'
toCardId: '11'
status: optional
---
L'agriculture est responsable d'émissions d'ammoniac, via l'utilisation d'engrais et le bétail. L'ammoniac joue ensuite un rôle sur la formation d'aérosols.
