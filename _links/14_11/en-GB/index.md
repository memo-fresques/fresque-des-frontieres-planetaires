---
fromCardId: '14'
toCardId: '11'
status: optional
---
Agriculture is responsible for ammonia emissions, via fertilizer use and livestock. Ammonia then plays a role in the formation of aerosols.
