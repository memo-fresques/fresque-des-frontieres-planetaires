---
fromCardId: '11'
toCardId: '20'
status: valid
---
Even though they have a shorter lifespan than GHGs, the continuous emission of aerosols increases the aerosol load in the atmosphere.
