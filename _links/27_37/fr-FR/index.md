---
fromCardId: '27'
toCardId: '37'
status: valid
---
Les modèles informatiques prédisent qu'une diminution de 10 % de la concentration d'ozone stratosphérique pourrait provoquer chaque année 300 000 cancers cutanés, 4 500 mélanomes et entre 1,60 million et 1,75 million de cas de cataracte de plus dans le monde.
Plus d'UVB peut entrainer aussi une baisse des défenses immunitaires.
