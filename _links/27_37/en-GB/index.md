---
fromCardId: '27'
toCardId: '37'
status: valid
---
Computer models predict that a 10% decrease in stratospheric ozone concentration could cause 300,000 more skin cancers, 4,500 melanomas and between 1.60 million and 1.75 million more cases of cataracts worldwide each year. .
More UVB can also lead to a drop in immune defenses.
