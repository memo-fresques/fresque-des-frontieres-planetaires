---
fromCardId: '29'
toCardId: '26'
status: valid
---
L'Amazonie contribue au régime des pluies via le phénomène d'évapotranspiration des arbres. Du fait de la superficie importante de l'Amazonie, sa savanisation pourrait entrainer une modification du régime des pluies localement, voire au niveau planétaire.
