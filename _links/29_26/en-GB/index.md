---
fromCardId: '29'
toCardId: '26'
status: valid
---
The Amazon contributes to the rain regime via the phenomenon of evapotranspiration from trees. Due to the large surface area of ​​the Amazon, its savannahization could lead to a change in the rain regime locally, or even globally.
