---
fromCardId: '8'
toCardId: '11'
status: optional
---
Concerning transport, there are emissions of aerosols through the wear of the braking systems.
For the use of buildings, a major source of aerosol emissions is wood heating, especially with low-efficiency fireplaces.
