---
fromCardId: '7'
toCardId: '30'
status: optional
---
Si on considère que cette carte inclu aussi les glaciers, alors il y a effectivement un impact sur les ressources en eau douce : les glaciers de l’Hindou Kouch et l’Himalaya fournissent de l’eau potable à 250 millions de personnes réparties dans 8 pays du monde. D’ici 2100, ils pourraient avoir fondu pour plus des 2/3 ce qui provoquera des inondations puis des pénuries d’eau douce : les grands fleuves que sont le Huang Hue (fleuve Jaune), le Yang-Tsé, le Brahmapoutre, L’Indus et le Gange ne charrieront plus beaucoup d’eau en été, eux qui alimentent le Bangladesh, le Pakistan et le nord de l’Inde.
