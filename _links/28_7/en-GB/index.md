---
fromCardId: '28'
toCardId: '7'
status: valid
---
The complete melting of Greenland would cause a rise of 7m. The complete melting of Antarctica would cause a rise of 54m.
If it is "only" West Antarctica, then this could result in a rise of 2-5m. Or a total elevation "Greenland + West Antarctica" of 9 to 12m in the event of total melting.
However, these figures correspond to very distant time scales well after 2100 (several hundreds, thousands of years). The projections for 2100 according to the IPCC are rather of the order of 50 to 80cm for the most probable, without excluding the beginning of a tipping point which could lead to an overall rise of 1.7m.
Since 2009, Antarctica has lost more than 250 billion tonnes of ice each year. In the 1980s, it was 40 billion tonnes (6 times less).
