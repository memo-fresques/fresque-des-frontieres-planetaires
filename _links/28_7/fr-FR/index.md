---
fromCardId: '28'
toCardId: '7'
status: valid
---
La fonte complète du Groenland entrainerait une élévation de 7m. La fonte complète de l'antarctique entrainerait une élévation de 54m.
Si il s'agit ""uniquement"" de l'antarctique de l'ouest, alors cela pourrait entrainer une élévation de 2-5m. Soit une élévation totale ""groenland + antarctique de l'ouest"" de 9 à 12m en cas de fonte totale.
Pour autant, ces chiffres correspondent à des échelles de temps très lointaines bien après 2100 (plusieurs centaines, milliers d'années). Les projections à l'horizon 2100 d'après le GIEC sont plutôt de l'ordre de 50 à 80cm pour les plus probables, sans exclure un début de point de bascule qui pourrait entrainer une élévation globale de 1.7m.
Depuis 2009, l’Antarctique perd chaque année + de 250 milliards de tonnes de glace. Dans les années 1980, c’était 40 milliards de tonnes (6 fois moins).
