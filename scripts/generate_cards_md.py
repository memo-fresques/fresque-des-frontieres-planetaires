import os
for k in range(41):
    try:
        os.mkdir(os.path.join('_cards', str(k)))
    except:
        pass
    with open(os.path.join('_cards', str(k), 'fr-FR.md'), 'w') as f:
        f.write('---\n')
        f.write(f'title: Carte {k}\n')
        f.write(f'backDescription:\n')
        f.write(f'lot: 0\n')
        f.write(f'num: {k}\n')
        f.write('---\n')