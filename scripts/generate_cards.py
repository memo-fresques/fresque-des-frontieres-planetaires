
import os
import sys
file_path = os.path.realpath(__file__)

script_dir = os.path.dirname(file_path)
sys.path.insert(0, os.path.join(script_dir, '../memo-viewer/scripts/utils'))
from cardsmanipulation import split_pdf, export_and_compress_svg, convert_pdf_to_images


if __name__ == '__main__':

    if len(sys.argv) != 2:
        print('python3 generate_cards.py <path_to_pdf_folder>')
        sys.exit(1)

    rename_map = { }

    for k in range(1, 40):
        rename_map[str(-1 + 2*k)] = f'{k}-front'
        rename_map[str(0 + 2*k)] = f'{k}-back'

    path_to_pdf_folder = sys.argv[1]
    cards_folder = os.path.join(script_dir, '..', 'cards')
    if not os.path.exists(cards_folder):
        os.mkidr(cards_folder)

    warnings = []
    languages = os.listdir(path_to_pdf_folder)
    for pdf_filename in languages:
        language = pdf_filename[:-4]

        path_to_pdf = os.path.join(path_to_pdf_folder, pdf_filename)

        result_folder = os.path.join(cards_folder, language)
        if os.path.exists(result_folder):
            warnings.append(
                f'Skip language { language } as folder already exists')
            continue
        print(f'> Generate language { language }')
        os.mkdir(result_folder)
        pdf_folder = os.path.join(result_folder, 'pdf')
        print('>> Split pdf')
        split_pdf(path_to_pdf, rename_map, pdf_folder)

        print('>> Export and compress SVG')
        export_and_compress_svg(pdf_folder)

        print('>> Export and compress SVG')
        convert_pdf_to_images(pdf_folder)

    print('WARNINGS')
    for warning in warnings:
        print('* ' + warning)
