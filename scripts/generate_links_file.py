import os
import frontmatter  # pip install python-frontmatter
import shutil

file_path = os.path.realpath(__file__)
script_dir = os.path.dirname(file_path)

links_dir = os.path.join(script_dir, '..', '_links')
DEFAULT_LANG = 'fr-FR'

links = [
    (1, 29),
    (1, 6, "Le déréglement climatique entraine l'augmentation de la fréquence et/ou de l'intensité des aléas climatiques : canicules, cyclones, sécheresses, crues, ..."),
    (1, 31, """Le corail est une symbiose entre des colonies de polypes et des algues photosynthétiques, les zooxanthelles, qui donnent sa belle couleur au corail.

Si la température de l'eau augmente, le corail expulse ses micro-algues (la symbiose est cassée). Dépigmenté, il laisse voir son squelette blanc.

Si cela se prolonge, le corail meurt, et des algues filamenteuses recouvrent son squelette."""),
    (1, 7, """Le changement climatique réchauffe l'eau des mers. De ce fait, l'eau se dilate, prend plus de volume, et contribue à la montée des océeans."""),
    (1, 28, """Le dérèglement climatique entraine une fonte des calottes glaciaires (= glaciers de très grande superficie). Un point de bascule entrainant la disparition complète du Groenland et de l'Antartique de l'Ouest n'est pas établi, mais pas impossible.
L'appellation exacte est Inlandsis pour l'antarctique et le Groenland (superficie > VVVkm2). Les calottes glaciaires sont....
Il n'est pas question ici des glaciers, même si ils sont aussi impactés par de réchaufement climatique."""),
    (1, 34),
    (1, 30, "", "optional"),

    (2, 37, """La distinction UVA/UVB/UVC tient à une plage de longueur d'ondes différente. Les UVC n'atteignent jamais la surface terrestre, ils sont entièrement filtrés par l'atmosphère. Les UVB et UVA traversent en partie l'atmosphère en fonction de la concentration en azote, oxygène, dioxygène et ozone. Ce dernier est le plus influant sur la filtration des UVB."""),
    (2, 1, """La couche d'ozone filtre une partie des UV, donc contribue à limiter l'énergie qui arrive à la surface de la terre. Cependant, cet impact n'est pas prépondérant.""", "optional"),

    (3, 1),
    (3, 19, """Les océans ont absorbés environ 1/4 des émissions de CO2."""),
    (3, 2, "", "invalid"),

    (4, 8, """L'industrie, le transport, le logement sont des activités humaines qui répondent à des besoins (se loger, se déplacer, communiquer, se divertir, ...). Ces activitités humaines nécessitent des ressources : énergie (dont plus de 80% est d'origine fossile) et matières dont beaucoup de métaux et minéraux"""),
    (4, 14, """L'agriculture est l'une des premières activités humaines répondant à un besoin fondamental : se nourrir."""),

    (5, 3, """La combustion imparfaite des énergies fossiles entraine des émissions de CO2 dans l'atmosphère.
Le CO2 représente plus de 70% des GES anthropiques au niveau mondial."""),
    (5, 9, """La fabrication du plastique "conventionel" repose entièrement sur l'exploitation du pétrole. A l'origine, le plastique est un "sous-produit"."""),
    (5, 11, """La combustion imparfaite des énergies fossiles entraine des émissions d'aérosols dans l'atmosphère."""),
    (5, 12, """Le méthane représente 17% des GES au niveau mondial (principalement agriculture, mais aussi lors de la production (fuites notamment) et combustion d'énergies fossiles). Le protoxyde d'azote n'est pas directement liés à l'utilisation des énergies fossiles."""),
    (5, 16),

    (6, 33),
    (6, 39, """Destruction de cultures par des tempètes, des cyclones, de la grèle, des inondations, des canicules, des sécheresses, ..."""),

    (7, 35, """Pour une hausse du niveau des mers de xx cm, cela pourrait entrainer le déplacement de XX millions de personnes (des grandes villes comme YYYY ou ZZZZ se trouvent à moins de GGG cm d'altitude)."""),
    (7, 39, """Les delta sont des zones très fertiles. La montée du niveau des mers entraine la salinisation de ces zones agricoles, et ont donc un impact sur les rendements agricoles.""", "optional"),

    (8, 5),
    (8, 10, """Les gaz chlorés sont par exemples les CFC utilisés dans les systèmes de refroidissements (réfrigérateurs, climatiseurs…), ou comme gaz propulseur dans les bombes aérosols, à ne pas confondre avec les "aérosols" (terme technique) qui sont eux des particules fines en suspension dans l'air."""),
    (8, 17, """L'industrie consomme 20 % d'eau douce, en 2è place après l'agriculture (loin devant avec 70%).""", "optional"),

    (9, 18, """Il pleut du plastique. Une étude effectuée dans les Pyrénées montre le dépôt de 365 micro-particules de plastique par m2 par jour (publiée dans Nature en 2019)

8 millions de tonnes : c'est la quantité de plastique polluant les écosystèmes marins chaque année selon la Fondation Ellen MacArthur. C'est l'équivalent d'un camion poubelle déchargé en mer chaque minute. Si rien n'est mis en œuvre, ce nombre passera à deux par minute d'ici à 2030, et à quatre par minute d'ici à 2050, précise la Fondation"""),
    (9, 37, """D'après étude de l'Université de Newcastle, un être humain pourrait ingérer environ 5 grammes de plastique chaque semaine soit l'équivalent de la quantité de microplastiques contenue dans une carte de crédit.""", "optional"),
    (10, 2, """L'ozone stratosphérique (qui constitue la couche d'ozone dans les hautes couches de l'atmosphère entre 20 et 40 km d'altitude), représente 90% de l'ozone présent dans l'atmosphère, et filtre 97% des rayons UVC. Il se forme par combinaison d'une molécule de dioxygène (O2) avec un atome isolé d'oxygène (c’est le rayonnement solaire qui crée ces atomes isolés en brisant les molécules d’oxygène en 2). Les gaz chlorés (comme les CFC, utilisés notamment comme gaz propulseur dans les bombes aérosols) qui arrivent dans la stratosphère sont décomposés par les UV et libèrent des atomes de chlore qui vont réagir avec l’ozone en le transformant en dioxygène.

Des trous dans la couche d'ozone ont été dans un premier temps limité à l'Antarctique, pour être ensuite observé à une moindre échelle sur le pôle Nord. Aujourd'hui, la Nouvelle Zélande et le Sud de l'Australie, mais également le Canada, la Scandinavie connaissent régulièrement de fortes baisses d'ozone stratosphérique au printemps.
Les conditions météorologiques sont différentes entre le pole sud et le pole nord, ce qui pourrait expliquer pourquoi le phénomène est plus marqué en Antartique (sud) qu'en arctique (nord). Notament parce qu'il y a plus de renouvellement des masses d'air au pole nord, alors que dans l'hémisphère sud, des acourants aériens "isole" les masses d'air, s'en suit un brassage moins important, et donc pas d'apport d'ozone depuis les régions équatoriales.

Les gouvernements ont décidé de mettre fin à la production des CFC dans le cadre du Protocole de Montréal qui est entré en vigueur le 1er janvier 1989. Ils sont remplacés dans un premier temps par des HCFC (qui sont moins destructeurs d'ozone), puis par des HFC dans un 2è temps (ils ne contiennent pas de chlore, donc ne vont pas réagir avec l'ozone)."""),

    (10, 1, """Les CFC sont des GES, et donc contribuent au dérèglement climatique. pour autant, leur impact global est inférieur à celui du CO2 / CH4 / N2O (bien qu'ayant un PRG parfois bien supérieur ).
Leurs remplacants (HFC, qui ne contiennent plus de Chlore et ne sont donc plus destructeurs d'Ozone) sont des GES encore plus puissants que les CFC.... """, "optional"),

    (11, 1, """Ces aérosols ont des causes naturelles (éruptions volcaniques ou humaines (activités industrielles, transport, agriculture)
A la différence des GES les aérosols ont au global un forçage radiatif négatif : ils ont tendance à refroidir le climat.
En effet les aérosols vont (1) réfléchir les rayons du soleil directement et (2) contribuer à la formation des nuages qui vont eux aussi réfléchir les rayons du soleil"""),
    (11, 20, """Même s'ils ont une durée de vie moins importante que les GES, l'émission continue d'aérosols augmente la charge en aérosols de l'atmosphère"""),

    (12, 1, """CH4 et N2O sont les principaux GES avec le CO2 en terme d'impact. Ils ont un PRG supérieur au CO2, mais sont émis en quantités bien inférieures."""),

    (13, 3, """La déforestation (première causes de destruction des habitats) conciste dans la plupart des cas à bruler les arbres sur place, ce qui relarge très rapidement le carbone qui avait été stocke lors de la croissance des arbres. Cela représente une contribution de 10% des émissions modiales de CO2."""),

    (13, 21, """La destruction des habitats est la première cause de disparition de la biodiversité."""),
    (13, 22),
    (13, 24),

    (14, 12, """La digestion des ruminants (bovins, caprins) dégage du méthane via le processus de fermentation entérique. La matière organique est décomposée sans oxygène. Le même processus a lieu dans les rizières.

Le N2O est issu de l'épandage d'engrais azotés. La partie des engrais qui ne va pas dans la plante pour la faire pousser s'oxyde à l'air libre."""),
    (14, 13, """L'agriculture nécessite de libérer des surfaces dédiées à l'alimentation humaine directe (ou indirecte via l'alimentation animale et les paturages). Cette libération de surface conduit à détruire des habitats naturels notamment au travers de la déforestation"""),
    (14, 15, """L'agriculture intensive surexploite les milieux au delà de sa capacité de régénération, et notamment le sol"""),
    (14, 16, """L'agriculture conventionnelle nécessite de grandes quantités d'engrais, notamment de l'azote synthétisé avec du gaz fossile, et du phosphore extrait de mines"""),
    (14, 17, """L'agriculture est le premier consommateur d'eau douce avec 70% de la consommation totale"""),

    (15, 21),

    (16, 12, """L'épendage des engrais azotés est responsable de la plupart des émissions de protoxyde d'azote (N2O)"""),
    (16, 23, """Les engrais à base de phosphore augmentent les quantités de phosphore relargué dans les sols."""),

    (17, 24),

    (18, 33),

    (19, 25),
    (19, 31),
    (19, 33, "", 'invalid'),

    (20, 26),
    (20, 37, """Les particules fines (PM10, PM2,5) se retrouvent en grande quantité dans les zones urbaines et pénètrent dans nos poumons. La pollution atmosphérique est responsable de 50 à 60 000 décès prématurés en France, et 91% de la population mondiale vit dans des endroits où les lignes directrices de l’OMS relatives à la qualité de l’air ne sont pas respectées (étude de 2016)."""),

    (21, 33),

    (22, 26),
    (22, 27),

    (23, 32),

    (24, 30),

    (25, 33),

    (26, 30),

    (27, 33),
    (27, 37, """Les modèles informatiques prédisent qu'une diminution de 10 % de la concentration d'ozone stratosphérique pourrait provoquer chaque année 300 000 cancers cutanés, 4 500 mélanomes et entre 1,60 million et 1,75 million de cas de cataracte de plus dans le monde."""),

    (28, 7, """La fonte complète du Groenland entrainerait une élévation de 7m. La fonte complète de l'antarctique entrainerait une élévation de 54m.
Si il s'agit "que" de l'antarctique de l'ouest, alors cela pourrait entrainer une élévation de 2-5m. Soit une élévation totale "groenland + antarctique de l'ouest" de 9 à 12m en cas de fonte totale.
Pour autant, ces chiffres correspondent à des échelles de temps très lointaines (>2100). Les projections à l'horizon 2100 d'après le GIEC sont plutôt de l'ordre de 50 à 80cm pour les plus probables, sans exclure un début de point de bascule qui pourrait entrainer une élévation globale de 1.7m."""),
    (28, 17, """Si on considère que cette carte inclu aussi les glaciers, alors il y a effectivement un impact sur les ressources en eau douce (glaciers himalayens).""", "optional"),

    (29, 22),
    (29, 33),
    (29, 1),
    (29, 26),

    (30, 39, """L'eau douce est indispensable à l'agriculture, que ce soit l'eau bleue ou l'eau verte."""),

    (31, 33, """25% de la biodiversité marine dépend des récifs coralliens."""),

    (32, 34, """L’eutrophisation des milieux aquatiques est un déséquilibre du milieu provoqué par l'augmentation de la concentration d’azote et de phosphore dans le milieu. Elle est caractérisée par une croissance excessive des plantes et des algues due à la forte disponibilité des nutriment. Les algues qui se développent grâce à ces substances nutritives absorbent de grandes quantités d'oxygène, lorsqu'elles meurent et se décomposent. Leur prolifération provoque l'appauvrissement, puis la mort de l'écosystème aquatique présent : il ne bénéficie plus de l'oxygène nécessaire pour vivre, ce phénomène est appelé « asphyxie des écosystèmes aquatiques »"""),

    (33, 39, """Une baisse des insectes polinisateurs peut impacter les rendement agricoles (75% de la production mondiale de nourriture dépend des insectes pollinisateurs).
Une baisse des coraux et planctons peut impacter les ressources halieutiques (poissons, crustacés, mollusques).
En Asie, plus d’un milliard de personnes dépendent de la pêche comme première ressource alimentaire (en protéines animales), en particulier dans les pays à faibles revenus."""),
    (33, 37, "", "optional"),

    (34, 33, """En janvier 2018, le groupe de travail international « Global ocean oxygen network » indique dans la revue Science que la proportion de zones de haute mer dépourvues de tout oxygène a plus que quadruplé en 50 ans (150 à 450 zones mortes recencées). Les sites faiblement oxygénés situés près des côtes (incluant les estuaires) ont été décuplés depuis 1950. Ces zones ont des impacts de plus en plus importants sur la pêche et les écosystèmes. Les poissons peuvent arriver à s'échapper de ces zones( mais perte de connaissance rapide possible). Par contre, les mollusques et crustacés ont une vitesse de déplacement trop faible, et meurent rapidement."""),

    (35, 36),
    (35, 37),
    (35, 38),

    (36, 35),
    (36, 37),
    (36, 38),

    (37, 35),
    (37, 36),
    (37, 38),

    (39, 36)
]

for link in links:
    print(len(link))
    link_id = f'{ link[0] }_{ link[1] }'
    link_dir = os.path.join(links_dir, link_id)
    if os.path.exists(link_dir):
        continue
        # shutil.rmtree(link_dir)
    os.mkdir(link_dir)
    with open(os.path.join(link_dir, 'index.md'), 'w') as f:
        f.write('')


    link_data = frontmatter.Post(content=link[2] if len(link) >= 3 else '')
    link_data['linkId'] = link_id
    link_data['fromCardId'] = link[0]
    link_data['toCardId'] = link[1]
    link_data['status'] = link[3] if len(link) >= 4 else 'valid'
    os.mkdir(os.path.join(link_dir, DEFAULT_LANG))
    with open(os.path.join(link_dir, DEFAULT_LANG, 'index.md'), 'w') as f:
        f.write(frontmatter.dumps(link_data))


