---
backDescription: >-
  350 000 nouvelles entités chimiques (produits de synthèse, plastiques, OGM,
  perturbateurs endocriniens…) ont été créées par l’espèce humaine. Leurs effets
  sur le système Terre n’est pas bien connu et leur usage est mal régulé. La
  frontière est située à zéro relargage de nouvelles entités à moins qu’elles
  soient inoffensives et surveillées. 

  La frontière est donc considérée comme franchie.
title: Nouvelles pollutions chimiques
---
Plastiques, pesticides, glyphosate, OGM, peintures, antibiotiques, médicaments, nanoparticlues, métaux lourds, composés radioactifs, …
Deux familles particulières : les Polluants Organiques Persistants (POP) qui sont des substances NON-biodégradables et qui transitent tout au long de la chaine alimentaire. Les perturbateurs endocriniens qui sont des substances qui onfluent sur le fonctionnement hormonal.
Sur les 150 000 substances chimiques recencées en Europe, seulement 3000 ont fait l'objet d'une évaluation de leurs imacts sur l'environnement et la santé.
Entre 1930 et 2000, la production mondiale de substances chimiques a été multipliée par 500 (la moitié étant des plastiques, et un tiers sont des engrais minéraux).
