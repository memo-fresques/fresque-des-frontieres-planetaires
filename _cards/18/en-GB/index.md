---
backDescription: >-
  350,000 new chemical entities (synthetic products, plastics, GMOs,
  endocrine disruptors, etc.) were created by the human species. Their effects
  on the Earth system is not well known and their use is poorly regulated. There
  border is located at zero release of new entities unless they
  are harmless and monitored.


  The border is therefore considered to have been crossed.
title: New chemical pollution
---
Plastics, pesticides, glyphosate, GMOs, paints, antibiotics, drugs, nanoparticles, heavy metals, radioactive compounds, etc.
Two particular families: Persistent Organic Pollutants (POPs) which are NON-biodegradable substances and which pass throughout the food chain. Endocrine disruptors which are substances that influence hormonal functioning.
Of the 150,000 chemical substances identified in Europe, only 3,000 have had their impacts on the environment and health assessed.
Between 1930 and 2000, global production of chemicals increased 500-fold (half being plastics, and a third being mineral fertilizers).
