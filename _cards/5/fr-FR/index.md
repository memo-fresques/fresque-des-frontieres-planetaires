---
backDescription: >-
  Les énergies fossiles (charbon, pétrole et gaz) sont issues de restes de vie
  (plantes, algues…) enfouis et décomposés sous terre.

  Elles comptent pour 80% de l’énergie mondiale consommée.
title: Utilisation des énergies fossiles
---
Les énergies fossiles (charbon, pétrole et gaz) sont issues de restes de vie (plantes, algues…) enfouis et décomposés sous terre.
Elles comptent pour 80% de l’énergie mondiale consommée.
