---
backDescription: >-
  Fossil fuels (coal, oil and gas) come from the remains of life
  (plants, algae, etc.) buried and decomposed underground.


  They account for 80% of global energy consumed.
title: Use of fossil fuels
---
Fossil fuels (coal, oil and gas) come from remains of life (plants, algae, etc.) buried and decomposed underground.
They account for 80% of global energy consumed.
