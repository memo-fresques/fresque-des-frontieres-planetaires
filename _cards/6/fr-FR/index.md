---
backDescription: >-
  Le changement climatique se manifeste déjà de manières très diverses et
  l'intensité de ces phénomènes va s'accentuer avec l’augmentation de la
  température.

  Par exemple, les canicules vont être plus importantes et plus longues, le
  régime des pluies (sécheresses, pluies intenses) sera modifié en profondeur et
  l'intensité des ouragans va augmenter. 

  Ces aléas climatiques divers peuvent se conjuguer et aggraver ainsi leurs
  impacts.
title: Augmentation des aléas climatiques
---
Le changement climatique se manifeste déjà de manières très diverses et l'intensité de ces phénomènes va s'accentuer avec l’augmentation de la température.
Par exemple, les canicules vont être plus importantes et plus longues, le régime des pluies (sécheresses, pluies intenses) sera modifié en profondeur et l'intensité des ouragans va augmenter. 
Ces aléas climatiques divers peuvent se conjuguer et aggraver ainsi leurs impacts.
