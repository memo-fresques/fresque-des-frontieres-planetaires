---
backDescription: >-
  Climate change is already manifesting itself in very diverse ways and
  the intensity of these phenomena will increase with the increase in
  temperature.


  For example, heatwaves will be more severe and longer, the
  rainfall regime (droughts, intense rains) will be profoundly modified and
  the intensity of hurricanes will increase.


  These various climatic hazards can combine and thus aggravate their
  impacts.
title: Increase in climatic hazards
---
Climate change is already manifesting itself in very diverse ways and the intensity of these phenomena will increase with the increase in temperature.
For example, heatwaves will be greater and longer, the rain regime (droughts, intense rains) will be profoundly modified and the intensity of hurricanes will increase.
These various climatic hazards can combine and thus worsen their impacts.
