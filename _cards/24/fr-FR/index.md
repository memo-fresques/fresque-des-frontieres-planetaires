---
backDescription: >-
  Ce processus est subdivisé en 2 sous-processus : eau verte et eau bleue. 

  L’eau verte, mesurée par l’humidité des sols en zone racinaire, est nécessaire
  aux écosystèmes terrestres. 

  L’eau bleue, eau des rivières et des nappes phréatiques, est vitale à
  l’intégrité des écosystèmes aquatiques.

  Les pressions humaines (agriculture intensive, changement climatique,
  déforestation) placent ces 2 processus dans la zone de risque grandissant.
title: Perturbation du cycle de l’eau douce
---
Pour l'eau bleue, la variable d'intérêt est le débit d'eau. La frontière est dépassée si le débit d'eau est "perturbé" (trop éloigné de la référence pré-industrielle) pour un trop grand pourcentage des surfaces terrestres. Pour l'eau verte, la variable d'intérêt est l'humidité des sols dans la zone racinaire. De la même manière, la frontière est dépassée si l'humidité de la zone racinaire est est "perturbé" (trop éloigné de la référence pré-industrielle) pour un trop grand pourcentage des surfaces terrestres.
