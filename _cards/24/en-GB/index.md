---
backDescription: >-
  This process is subdivided into 2 sub-processes: green water and blue water.


  Green water, measured by soil moisture in the root zone, is necessary
  to terrestrial ecosystems.


  Blue water, water from rivers and groundwater, is vital to
  the integrity of aquatic ecosystems.


  Human pressures (intensive agriculture, climate change,
  deforestation) place these two processes in the zone of growing risk.
title: Disruption of the freshwater cycle
---
For blue water, the variable of interest is water flow. The boundary is exceeded if the water flow is "disturbed" (too far from the pre-industrial reference) for too large a percentage of land surfaces. For green water, the variable of interest is soil moisture in the root zone. In the same way, the boundary is exceeded if the humidity of the root zone is "disturbed" (too far from the pre-industrial reference) for too large a percentage of the land surfaces.
