---
backDescription: >-
title: Biogeochemical flows (nitrogen and phosphorus)
---
"Le Reveilleur videos on Phosphorus: https://www.youtube.com/watch?v=gqfXMDK3dnE
For nitrogen, the challenge is to prevent excessive release of reactive nitrogen into water and natural aquatic environments in order to avoid their eutrophication. We therefore measure the fixation of dinitrogen by industry and agriculture. The threshold not to be exceeded for nitrogen is set at 62 teragrams per year (Tg N/year).
Nitrogen is abundant but inactive in the atmosphere. To play its role, it must be made active (we call this "fixation"), either naturally via bacteria, or by an industrial process. The introduction of additional flows of nitrogen into the natural cycle leads to saturation of natural environments, and we find these surplus nitrogen in the atmosphere via nitrous oxide (powerful GHG) and in rivers.


For phosphorus, the challenge is to avoid an episode of strong reduction of oxygen in the oceans. At the global level (asphyxiation of the oceans), the threshold is estimated at 11 teragrams per year (Tg P/year) of phosphorus released into water (it must not be more than ten times higher than natural discharge).
In addition, phosphorus is an exhaustible resource from the mining industry (depletion expected during this century)."
