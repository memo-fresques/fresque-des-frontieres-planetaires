---
backDescription: "L’usage massif des engrais agricoles contenant de l’azote et du phosphore a permis d’augmenter la production alimentaire. Une partie de ces engrais ne reste pas dans les champs et pollue rivières et océans. Ces flux d’azote et de phosphore sont au moins 2 fois supérieurs au seuil de la zone sûre\_ : ce processus est dans la zone de risque important."
title: Flux biogéochimiques (azote et phosphore)
---
"Vidéos le Reveilleur sur le Phosphore : https://www.youtube.com/watch?v=gqfXMDK3dnE
Pour l’azote, l’enjeu est d’empêcher un rejet excessif d’azote réactif dans l’eau et les milieux naturels aquatiques afin d’éviter leur eutrophisation. On mesure donc la fixation de diazote par l’industrie et l’agriculture. Le seuil à ne pas dépasser pour l’azote est fixé à 62 téragrammes par an (Tg N/an).
L'azote est abondant mais inactif dans l'atmosphère. Pour jouer son role, il doit etre rendu actif (on appelle ça la ""fixation""), soit de manière naturelle via des bactéries, soit par une procédé industriel. L'introduction de flux supplémentaires d'azote dans le cycle naturel entraine une saturation des milieux naturels, et on retrouve ces surplus d'azote dans l'atmosphère via le protoxyde d'azote (puissant GES) et dans les rivières.

Pour le phosphore, l’enjeu est d’éviter un épisode de forte réduction d’oxygène dans les océans. Au niveau mondial (asphyxie des océans), le seuil est estimé à 11 téragrammes par an (Tg P/an)  de phosphore rejetées dans l’eau (il ne doit pas être plus de dix fois supérieur au rejet naturel).
De plus le phosphore est une ressource épuisable issu de l'industrie minière (épuisement prévu au cours de ce siècle)."
