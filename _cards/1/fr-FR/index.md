---
backDescription: >-
  Un climat stable pendant les 10 derniers millénaires a permis un cadre propice
  au développement des civilisations humaines. En 2022, la Terre s’est
  réchauffée de +1,15°C par rapport à la période préindustrielle. Ce processus
  est entre la zone de risque grandissant et important. Le phénomène est
  irréversible à l’échelle d’une vie humaine.
title: Changement climatique
---
Depuis la révolution industrielle, la température sur Terre a augmenté à cause d'un renforcement de l'effet de serre. L'effet de serre est un phénomène naturel par lesquels des gaz dits "gaz à effet de serre" permettent de conserver une partie de la chaleur infrarouge émise par la Terre, qui ne repart pas directement vers l'atmosphère. Sans effet de serre, il ferait en moyenne -18°C sur Terre (contre environ 15°C actuellement) ! Mais avec l'émission de gaz à effet de serre par les activités humaines, leur concentration dans l'atmosphère augmente, ce qui augmente l'effet de serre, et réchauffe. En 2022, la Terre s'est réchauffée de +1,15°C par rapport à la période préindustrielle. 


Si on vous demande en animation pourquoi le processus changement climatique est entre la zone de risque grandissant et important, vous pouvez donner plus de détails. Pour le processus changement climatique, le Stockholm Resilience Centre (SRC) a défini 2 variables de contrôle : la concentration en CO2 atmosphérique et le forçage radiatif. Pour l'une des variables de contrôle (le CO2) le changement climatique est en zone de risque grandissant. Pour l'autre variable de contrôle (le forçage radiatif), le changement climatique est en zone de risque important. D'où le placement entre les deux zones. Le CO2 est le gaz à effet de serre qui contribue le plus à l'effet de serre additionnel. Le forçage radiatif mesure la différence entre l'énergie qui arrive sur Terre et celle qui repart chaque seconde. Un forçage radiatif positif signifie qu'il y a plus d'énergie qui arrive qui ne repart, ce qui conduit à un réchauffement.
