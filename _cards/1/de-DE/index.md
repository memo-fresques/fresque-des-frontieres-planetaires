---
title: "Klimawandel"
backDescription: ""
---
Ein stabiles Klima während der letzten 10.000 Jahre sorgte für günstige Rahmenbedingungen für die Entwicklung der menschlichen Zivilisation. Im Jahr 2022 hat sich die Erde im Vergleich zum vorindustriellen Zeitalter um +1,15°C erwärmt. Dieser Prozess liegt im Bereich zwischen wachsendem und erheblichem Risiko. Das Phänomen ist im Zeitraum eines Menschenlebens nicht umkehrbar.
