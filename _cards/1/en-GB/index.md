---
backDescription: >-
  A stable climate over the last 10 millennia has provided a favorable environment
  to the development of human civilizations. In 2022, the Earth will
  warmed by +1.15°C compared to the pre-industrial period. This process
  is between the zone of increasing and significant risk. The phenomenon is
  irreversible on the scale of a human life.
title: Climate change
---
Since the industrial revolution, the temperature on Earth has increased due to a strengthening of the greenhouse effect. The greenhouse effect is a natural phenomenon by which so-called "greenhouse gases" allow part of the infrared heat emitted by the Earth to be retained, which does not return directly to the atmosphere. Without the greenhouse effect, it would be -18°C on average on Earth (compared to around 15°C currently)! But with the emission of greenhouse gases by human activities, their concentration in the atmosphere increases, which increases the greenhouse effect, and heats up. In 2022, the Earth will have warmed by +1.15°C compared to the pre-industrial period.




If you are asked in animation why the climate change process is between the zone of growing and significant risk, you can give more details. For the climate change process, the Stockholm Resilience Center (SRC) has defined 2 control variables: atmospheric CO2 concentration and radiative forcing. For one of the control variables (CO2) climate change is in a zone of growing risk. For the other control variable (radiative forcing), climate change is in a significant risk zone. Hence the placement between the two zones. CO2 is the greenhouse gas that contributes the most to the additional greenhouse effect. Radiative forcing measures the difference between the energy arriving on Earth and that leaving each second. A positive radiative forcing means that there is more energy coming in than leaving, which leads to warming.
