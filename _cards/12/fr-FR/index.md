---
backDescription: >+
  Le méthane est le 2ème GES anthropique (gaz à effet de serre émis par les
  humains).

  Il est produit principalement par les ruminants (vaches, moutons ou chèvres)
  et les fuites de méthane de l’industrie pétrogazière.

  Le protoxyde d’azote est le 3ème GES anthropique. Son premier poste
  d’émissions est l’utilisation d’engrais azotés agricoles. 

title: Emissions de méthane et de protoxyde d’azote
---
Après le CO2, les 2 autres GES majoritaires émis par l'espèce humaine sont le CH4 et le N20
