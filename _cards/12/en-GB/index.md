---
backDescription: >-
  Methane is the 2nd anthropogenic GHG (greenhouse gas emitted by
  humans).


  It is produced mainly by ruminants (cows, sheep or goats)
  and methane leaks from the oil and gas industry.


  Nitrous oxide is the 3rd anthropogenic GHG. His first position
  of emissions is the use of agricultural nitrogen fertilizers.


title: Methane and nitrous oxide emissions
---
After CO2, the 2 other major GHGs emitted by the human species are CH4 and N20
