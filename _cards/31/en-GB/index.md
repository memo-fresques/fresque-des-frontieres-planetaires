---
backDescription: >-
  Marine heat waves break the plant-animal symbiosis that forms
  corals (coral bleaching).


  Tropical corals are expected to decline by more than 99% in a +2°C world.


  Tropical corals are home to 25% of marine biodiversity.
title: Coral reef disappearance
---


