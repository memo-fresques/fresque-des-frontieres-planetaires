---
backDescription: >-
  Les vagues de chaleur marines cassent la symbiose végétal-animal que forment
  les coraux (blanchiment des coraux).

  Les coraux tropicaux devraient décliner de plus de 99% dans un monde à +2°C.

  Les coraux tropicaux abritent 25% de la biodiversité marine.
title: Disparition des coraux
---

