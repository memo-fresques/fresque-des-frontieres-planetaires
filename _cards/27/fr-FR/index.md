---
backDescription: |-
  Une couche d’ozone plus fine filtre moins les ultraviolets B.
  Leur impact est délétère pour les êtres vivants sur terre et dans la mer.
title: Impacts des rayons ultra-violets B
---

