---
backDescription: >-
  A thinner ozone layer filters less ultraviolet B.
  Their impact is harmful for living beings on land and in the sea.
title: Impacts of ultraviolet B radiations
---


