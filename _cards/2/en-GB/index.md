---
backDescription: >-
  Ozone (O3) present in the atmosphere at high altitude (> 10km), forms the “
  ozone layer” which has a protective role for living beings.


  CO2 and aerosols are not responsible for the hole in the diaper
  d’ozone.


  The ozone layer is reforming thanks to the ban on chlorine gases which
  disintegrated.


  This process has returned to the safe zone.
title: Stratospheric ozone depletion
---
Depending on its location in the atmosphere (at low or high altitude), ozone does not have the same effects. You may have heard of “good” and “bad” ozone. The ozone present at high altitude, in the stratosphere (20 to 50 kms) forms “the ozone layer” which filters and protects us from ultraviolet solar rays. This is "good ozone" and what we are talking about in this map. On the other hand, at low altitude, in the troposphere, ozone is present in small quantities and represents an air pollutant, as well as a short-lived greenhouse gas ("bad" ozone). It is a so-called “secondary” pollutant because it is not directly emitted by human activities but is formed by chemical reaction with other pollutants.
