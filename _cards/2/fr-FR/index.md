---
backDescription: >-
  L’ozone (O3) présent dans l’atmosphère à haute altitude (> 10km), forme la «
  couche d’ozone » qui a un rôle protecteur pour les êtres vivants.

  Le CO2 et les aérosols ne sont pas responsables du trou dans la couche
  d’ozone.

  La couche d’ozone se reforme grâce à l’interdiction des gaz chlorés qui la
  désagrégeaient.

  Ce processus est revenu dans la zone sûre.
title: Appauvrissement de la couche d’ozone
---
Selon sa localisation dans l'atmosphère (à basse ou haute altitude), l'ozone n'a pas les mêmes effets. Vous avez peut-être déjà entendu parler du "bon" et du "mauvais" ozone. L'ozone présent à haute altitude , dans la stratosphère (20 à 50 kms) forme « la couche d'ozone » qui filtre et nous protège des rayons solaires ultraviolets. C'est le "bon ozone" et de cela que nous parlons dans cette carte. En revanche, à basse altitude, dans la troposphère, l'ozone est présent en faible quantité et représente un polluant de l'air, ainsi qu'un gaz à effet de serre à faible durée de vie ("mauvais" ozone). Il s'agit d'un polluant dit "secondaire" car il n'est pas directement émis par les activités humaines mais se forme par réaction chimique avec d'autres polluants.
