---
backDescription: >-
  L’exploitation directe du vivant est la deuxième cause d'extinction d'espèces
  dans le monde.

  Elle est due à des pratiques agricoles intensives (utilisation des produits
  phytosanitaires, travail du sol) et à la surpêche.
title: Surexploitation des milieux
---

