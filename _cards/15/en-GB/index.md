---
backDescription: >-
  Direct exploitation of living things is the second cause of species extinction
  in the world.


  It is due to intensive agricultural practices (use of products
  phytosanitary measures, tillage) and overfishing.
title: Direct exploitation of organisms
---


