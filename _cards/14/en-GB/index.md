---
backDescription: >-
  The agricultural sector stands out from other sectors of activity because it impacts
  in depth of many planetary processes.


  It plays a major role in the consumption of fresh water and fertilizers.


  Per quantity of food produced, livestock farming has a footprint
  environmental impact more important than plant production.
title: Agriculture
---


