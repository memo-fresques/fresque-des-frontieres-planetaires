---
backDescription: >-
  Le secteur agricole se distingue des autres secteurs d'activité car il impacte
  en profondeur de nombreux processus planétaires.

  Il tient une place majeure dans la consommation d’eau douce et d’engrais.

  Par quantité de nourriture produite, l’élevage a une empreinte
  environnementale plus importante que les productions végétales.
title: Agriculture
---

