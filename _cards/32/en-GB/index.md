---
backDescription: >-
  Eutrophication is the process by which nutrients accumulate in
  an environment.


  This concerns in particular nitrogen and phosphorus.
title: Environment eutrophication
---
“Le Réveilleur video on eutrophication: https://www.youtube.com/watch?v=uGp3FuQnQWU
Eutrophication is well known in France through the phenomenon of green algae (particularly in Brittany)."
