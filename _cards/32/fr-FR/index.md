---
backDescription: >-
  L'eutrophisation est le processus par lequel des nutriments s'accumulent dans
  un milieu.

  Cela concerne notamment l'azote et le phosphore.
title: Eutrophisation des milieux
---
"Vidéo le Réveilleur sur l'eutrophisation : https://www.youtube.com/watch?v=uGp3FuQnQWU
L'eutrophisation est bien connue en France par le phénomène d’algues vertes (notamment en Bretagne)."
