---
backDescription: >-
  Forests have a vital role for humanity (water cycle, regulation of
  climate, etc.) and deforestation is a major global marker of
  change in land use.


  40% of the world's forests have been deforested and the border, located 25% of
  deforestation, is therefore crossed. This process is in the risk zone
  growing.
title: Land-system change
---
“Deforestation and land artificialization reduce plant cover. The contributions to land use change are as follows: 85% through land cultivation, 10% through urbanization, 5% through extraction raw material.
The annual deforestation of tropical forests represents twice the surface area of ​​Switzerland.
Exceeding this process is measured by the evolution of the forest area in the world compared to the year 1700. The border is set at 75% of the original forest cover, and it is crossed if less than 75% remain. %. Today we are at 60% remaining."
