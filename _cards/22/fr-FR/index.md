---
backDescription: >-
  Les forêts ont un rôle vital pour l’humanité (cycle de l’eau, régulation du
  climat…) et la déforestation est un marqueur majeur au niveau mondial du
  changement d’affectation des sols.

  40% des forêts mondiales ont été déforestées et la frontière, située à 25% de
  déforestation, est donc franchie. Ce processus est dans la zone de risque
  grandissant.
title: Changement d’affectation des sols
---
"La déforestation et l'artificialisation des sols réduisent le couvert végétal. Les contributions au changement d'affectation des sols sont le suivantes : 85% par la mise en culture des terres, 10% par l'urbanisation, 5% par l'extraction des matières premières.
La déforestation annuelle des forets tropicales représente 2 fois la superficie de la Suisse.
Le dépassement de ce processus est mesuré par l'évolution de la surface forestière dans le monde par rapport à l'année 1700. La frontière est fixée à 75% de la couverture forestière originelle, et elle est franchie s'il reste moins de 75%. Aujourd'hui nous en sommes à 60% restant."
