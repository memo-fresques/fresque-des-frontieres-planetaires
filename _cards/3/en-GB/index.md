---
backDescription: >-
  85% of CO2 emissions come from the combustion of fossil fuels
  and 15% of deforestation. They are the main cause of change
  climatic.
title: CO2 emissions
---
CO2 (carbon dioxide) is the main gas responsible for the additional greenhouse effect.
