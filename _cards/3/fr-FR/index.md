---
backDescription: >-
  Les émissions de CO2 sont issues à 85% de la combustion des énergies fossiles
  et à 15% de la déforestation. Elles sont la principale cause du changement
  climatique.
title: Emissions de CO2
---
Le CO2 (dioxyde de carbone) est le principal gaz responsable de l'effet de serre additionnel. 
