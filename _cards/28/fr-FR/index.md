---
backDescription: >-
  Les calottes du Groenland et de l'Antarctique de l'Ouest vont partiellement
  disparaître dans les siècles et millénaires à venir.

  Leur fonte complète ferait monter le niveau des mers jusqu’à 12 mètres.

  Le non-respect de l’Accord de Paris pourrait leur faire passer un point de
  bascule et leur fonte entière sur plusieurs millénaires.
title: Point de bascule pour les calottes glaciaires
---
Les calottes glaciaires (aussi dit Inlandsis) sont des glaciers d'une superficie > 50 000 km2. Sur Terre il y en a 2 : la calotte Antarctique et la calotte groenlandaise.
L'antarctique représente 90% des glaces terrestres (c'est la plus grande réserve d'eau duce de la planète), et a une épaisseur moyenne de plus de 2000m.
Le Groenland représente 10 % des ressources en eau douce, et à une épaisseur allant jusqu'à 3000m.
