---
backDescription: >-
  The ice sheets of Greenland and West Antarctica are partially
  disappear in the centuries and millennia to come.


  Their complete melting would cause sea levels to rise by up to 12 meters.


  Failure to comply with the Paris Agreement could put them past a point of
  rock and their complete melting over several millennia.
title: Tipping point for the ice caps
---
Ice caps (also called Inlandsis) are glaciers with an area > 50,000 km2. On Earth there are 2: the Antarctic ice sheet and the Greenland ice sheet.
Antarctica represents 90% of the Earth's ice (it is the largest reserve of water on the planet), and has an average thickness of more than 2000m.
Greenland represents 10% of fresh water resources, and at a thickness of up to 3000m.
