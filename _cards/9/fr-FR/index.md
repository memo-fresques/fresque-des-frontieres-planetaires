---
backDescription: >-
  La production de plastique a presque doublé en 20 ans au niveau mondial.

  Le plastique n'existe pas à l'état naturel, c'est un produit dérivé du
  pétrole.

  Depuis leur création, 79% des plastiques ont été jetés (en décharge ou dans
  l’environnement).
title: Relargage du plastique dans l’environnement
---
La production de plastique a presque doublé en 20 ans au niveau mondial.
Le plastique n'existe pas à l'état naturel, c'est un produit dérivé du pétrole.
Depuis leur création, 79% des plastiques ont été jetés (en décharge ou dans l’environnement).
