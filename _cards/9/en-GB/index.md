---
backDescription: >-
  Plastic production has almost doubled in 20 years globally.


  Plastic does not exist in its natural state, it is a product derived from
  oil.


  Since their creation, 79% of plastics have been thrown away (in landfill or in
  the environment).
title: Production and release of plastics into the environment
---
Plastic production has almost doubled in 20 years globally.
Plastic does not exist in its natural state, it is a product derived from petroleum.
Since their creation, 79% of plastics have been thrown away (in landfill or in the environment).
