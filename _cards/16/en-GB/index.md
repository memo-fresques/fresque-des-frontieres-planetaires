---
backDescription: >-
  Most nitrogen-based fertilizers come from a controlled reaction
  between methane (fossil gas) and nitrogen in the air.


  Phosphorus is extracted from mines to make fertilizer.


  Nitrogen and phosphate fertilizers promote plant growth.
title: Use of agricultural fertilisers
---


