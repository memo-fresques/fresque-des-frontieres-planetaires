---
backDescription: >-
  La plupart des engrais à base d’azote sont issus d’une réaction contrôlée
  entre du méthane (gaz fossile) et l’azote de l’air.

  Le phosphore est extrait de mines pour en faire des engrais.

  Les engrais azotés et phosphatés favorisent la pousse des plantes.
title: Utilisation des engrais agricoles
---

