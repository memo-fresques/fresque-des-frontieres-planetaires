---
backDescription: >-
  Les aérosols sont des petites particules qui restent en suspension, un certain
  temps, dans l’air (particules fines de moteur diesel, suies des éruptions
  volcaniques…).

  La combustion des énergies fossiles ou du bois émet des aérosols.

  Les aérosols refroidissent le climat et ne font pas de trou dans la couche
  d'ozone.
title: Emissions anthropiques d’aérosols
---
Les aérosols (particules solides ou liquides en suspension dans l'air) ont des causes naturelles ou humaines. Exemples de causes naturelles : éruptions volcaniques, pollens, tempête de sable. Exemples de causes humaines : activités industrielles / combustion d'énergie fossiles, transport (diesel, usure des pneus et freins), agriculture.
Les aérosols sont mauvais pour la santé et font partie de ce que l'on appelle "les particules fines".
