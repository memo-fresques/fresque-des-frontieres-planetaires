---
backDescription: >-
  Aerosols are small particles that remain suspended for some time.
  time, in the air (fine particles from diesel engines, soot from eruptions
  volcanic…).


  The combustion of fossil fuels or wood emits aerosols.


  Aerosols cool the climate and do not make a hole in the layer
  d'ozone.
title: Anthropogenic aerosol emissions
---
Aerosols (solid or liquid particles suspended in the air) have natural or human causes. Examples of natural causes: volcanic eruptions, pollen, sandstorms. Examples of human causes: industrial activities / combustion of fossil fuels, transport (diesel, tire wear and brakes), agriculture.
Aerosols are bad for your health and are part of what we call “fine particles”.
