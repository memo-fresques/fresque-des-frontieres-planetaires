---
backDescription: >-
  L'eutrophisation stimule la croissance d'algues qui, en absorbant l’oxygène
  présent dans l’eau, asphyxient le reste de leur écosystème (anoxie = pas
  d'oxygène).

  Par ailleurs, le changement climatique limite la dissolution de l’oxygène dans
  l’eau (hypoxie = manque d'oxygène) jusqu’à 300 mètres de profondeur.
title: Zones anoxiques ou hypoxiques
---

