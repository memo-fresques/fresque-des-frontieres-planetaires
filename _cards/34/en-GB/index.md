---
backDescription: >-
  Eutrophication stimulates the growth of algae which, by absorbing oxygen
  present in the water, suffocates the rest of their ecosystem (anoxia = no
  oxygen).


  Furthermore, climate change limits the dissolution of oxygen in
  water (hypoxia = lack of oxygen) up to 300 meters deep.
title: Anoxic or hypoxic areas
---


