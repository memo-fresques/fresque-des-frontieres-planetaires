---
backDescription: >-
  Les nuages se forment grâce aux aérosols. La modification de la teneur en
  aérosols dans l'air peut donc modifier le régime des pluies (et notamment la
  mousson).

  Cela peut avoir un impact sur les rendements agricoles.
title: Modification du régime des pluies
---

