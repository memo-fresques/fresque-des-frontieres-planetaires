---
backDescription: >-
  Clouds form thanks to aerosols. The modification of the content of
  aerosols in the air can therefore modify the rain regime (and in particular the
  monsoon).


  This can impact agricultural yields.
title: Changes in rainfall patterns
---


