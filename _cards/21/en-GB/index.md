---
backDescription: >-
  Biodiversity and the services it provides are vital for humanity.
  The integrity of the biosphere is notably assessed based on the rate of
  disappearance of living species, which is currently 100 to 1000 times more
  faster than the average observed over the last 10 million years. This
  process is in the significant risk zone.
title: Biodiversity integrity
---
"The threshold for the erosion of biodiversity is measured using 2 variables:


One of them is the rate of extinctions which must be less than 10 extinctions per year and per million species.


The rate of disappearance of living species on our blue planet is 10 to 100 times faster than the average observed over the last 10 million years, which places this border in the zone of significant risks. The losses are irreversible.


The main causes of biodiversity erosion are in order: Destruction of habitats (mainly deforestation), Overexploitation (mainly overfishing), climate change, pollution, proliferation of invasive species.
