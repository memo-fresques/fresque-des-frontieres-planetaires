---
backDescription: >-
  La biodiversité et les services qu’elle rend sont vitaux pour l’humanité.
  L’intégrité de la biosphère est notamment évaluée à partir du rythme de
  disparition des espèces vivantes, qui est actuellement 100 à 1000 fois plus
  rapide que la moyenne observée sur les 10 derniers millions d’années. Ce
  processus est dans la zone de risque important.
title: Intégrité de la biosphère
---
"Le seuil de l’érosion de la biodiversité se mesure à partir de 2 variables :

L'une d'entre elle est le taux d’extinctions qui doit être inférieur à 10 extinctions par an et par million d’espèces.

Le rythme de disparition des espèces vivantes sur notre planète bleue est 10 à 100 fois plus rapide que la moyenne observée sur les 10 derniers millions d’années ce qui place cette frontière dans la zone de risques importants. Les pertes sont irréversibles.

Les causes principales d'érosion de la biodiversité sont dans l'ordre : Destruction des habitats (principalement déforestation), Surexploitation (principalement surpêche), dérèglement climatique, les pollutions, la prolifération d'espèces invasives."
