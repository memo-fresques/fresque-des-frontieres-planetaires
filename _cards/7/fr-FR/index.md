---
backDescription: >-
  Le changement climatique est responsable de la dilatation des océans ainsi que
  de la fonte des glaciers et des calottes glaciaires.

  Cela élève le niveau des mers qui est déjà monté de 16 cm et atteindra en 2100
  entre +50 cm et +1,75 m en fonction de nos émissions futures.
title: Montée des eaux
---
Le changement climatique est responsable de la dilatation des océans ainsi que de la fonte des glaciers et des calottes glaciaires.
Cela élève le niveau des mers qui est déjà monté de 16 cm et atteindra en 2100 entre +50 cm et +1,75 m en fonction de nos émissions futures.
