---
backDescription: >-
  Climate change is responsible for the expansion of the oceans as well as
  melting glaciers and ice caps.


  This raises sea levels which have already risen by 16 cm and will reach in 2100
  between +50 cm and +1.75 m depending on our future emissions.
title: Sea-level rise
---
Climate change is responsible for the expansion of oceans as well as the melting of glaciers and ice caps.
This raises sea levels, which have already risen by 16 cm and will reach between +50 cm and +1.75 m in 2100 depending on our future emissions.
