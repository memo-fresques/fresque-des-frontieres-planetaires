---
backDescription: >-
  Among human activities, agriculture is the sector that consumes the
  more fresh water (70%) ahead of industry (20%) and domestic uses
  (10%).


  Global freshwater consumption is increasing and if the border
  planet is defined at the global level, regional disparities are
  important.
title: Freshwater consumption
---
The fresh water concerned by this card is blue water, that is to say that which can be pumped and used directly.
