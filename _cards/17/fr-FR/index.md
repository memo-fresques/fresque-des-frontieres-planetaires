---
backDescription: >-
  Parmi les activités humaines, l’agriculture est le secteur qui consomme le
  plus d’eau douce (70%) devant l'industrie (20%) et les usages domestiques
  (10%).

  La consommation d’eau douce mondiale est en hausse et si la frontière
  planétaire est définie au niveau global, des disparités régionales sont
  importantes.
title: Consommation d’eau douce
---
L'eau douce concernée par cette carte est l'eau bleue, c'est à dire celle que l'on peut pomper et utiliser directement.
