---
backDescription: >-
  La pression à la baisse sur les rendements agricoles et l’effondrement de la
  biodiversité marine peuvent engendrer des famines.
title: Famines
---

