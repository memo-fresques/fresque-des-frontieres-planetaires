---
backDescription: >-
  Les famines, les extrêmes climatiques mais aussi les aérosols, le trou dans la
  couche d’ozone et les nouvelles entités chimiques peuvent affecter la santé
  humaine.
title: Santé humaine
---

