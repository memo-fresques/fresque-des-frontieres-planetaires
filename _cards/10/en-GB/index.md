---
backDescription: >-
  Chlorofluorocarbons (CFCs) used in refrigerators or water pumps
  heat created the hole in the ozone layer.


  They were banned and replaced by HFCs (hydrofluorocarbons) which do not
  do not destroy the ozone present in the upper atmosphere.


  The ozone layer is expected to return to its initial state in 2080.


  CFCs and HFCs are powerful greenhouse gases.
title: Use of chlorinated gases
---
Plastic production has almost doubled in 20 years globally.
Plastic does not exist in its natural state, it is a product derived from petroleum.
Since their creation, 79% of plastics have been thrown away (in landfill or in the environment).
