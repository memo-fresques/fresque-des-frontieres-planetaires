---
backDescription: >-
  Les chlorofluorocarbures (CFC) utilisés dans les frigos ou les pompes à
  chaleur ont créé le trou dans la couche d’ozone.

  Ils ont été interdits et remplacés par les HFC (hydrofluorocarbures) qui ne
  détruisent pas l’ozone présent dans la haute atmosphère.

  La couche d'ozone devrait revenir à son état initial en 2080.

  Les CFC et les HFC sont de puissants gaz à effet de serre.
title: Utilisation des gaz chlorés
---
La production de plastique a presque doublé en 20 ans au niveau mondial.
Le plastique n'existe pas à l'état naturel, c'est un produit dérivé du pétrole.
Depuis leur création, 79% des plastiques ont été jetés (en décharge ou dans l’environnement).
