---
backDescription: >-
  Si l’océan est trop acide, certaines espèces de planctons (ptéropodes et
  coccolithophores) n’arrivent plus à former leur coquille et disparaissent.

  Le plancton est à la base de la chaîne alimentaire marine. 
title: Impacts sur les planctons
---

