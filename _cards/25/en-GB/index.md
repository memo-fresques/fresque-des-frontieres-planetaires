---
backDescription: >-
  If the ocean is too acidic, certain species of plankton (pteropods and
  coccolithophores) are no longer able to form their shell and disappear.


  Plankton are at the base of the marine food chain.
title: Impacts on plankton
---


