---
backDescription: >-
  Ces activités humaines utilisent notamment des énergies fossiles, des gaz
  fluorés, du plastique et de l'eau douce.
title: Industrie / Transport / Logement
---
Ces activités humaines utilisent notamment des énergies fossiles, des gaz fluorés, du plastique et de l'eau douce.
