---
backDescription: >-
  L’affaiblissement des services rendus par la nature, les aléas climatiques et
  les modifications des ressources en eau douce peuvent avoir un effet
  défavorable sur les rendements agricoles.

  Les ressources de poissons vont baisser à cause des stress sur les coraux, sur
  le plancton et des zones hypoxiques et anoxiques.
title: Baisse des rendements agricoles et de la pêche
---

