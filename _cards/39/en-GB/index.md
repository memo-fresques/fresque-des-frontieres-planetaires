---
backDescription: >-
  The weakening of services provided by nature, climatic hazards and
  changes in freshwater resources can have an effect
  unfavorable for agricultural yields.


  Fish resources will decline due to stress on corals,
  plankton and hypoxic and anoxic zones.
title: Decline in agricultural and fisheries yields
---
