---
backDescription: >-
  La poursuite de la déforestation amazonienne combinée à un climat qui se
  réchauffe augmente la probabilité que cet écosystème passe un point de bascule
  pendant le XXIème siècle.

  L'Amazonie deviendrait alors une savane.

  La savanisation de l’Amazonie accentuerait en retour le changement climatique,
  modifierait le régime des pluies et impacterait la biodiversité.
title: Savanisation de l’Amazonie
---

