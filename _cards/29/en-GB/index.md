---
backDescription: >-
  Continued Amazonian deforestation combined with a climate that is
  warming increases the likelihood that this ecosystem will pass a tipping point
  during the 21st century.


  The Amazon would then become a savannah.


  The savannahization of the Amazon would in turn accentuate climate change,
  would modify the rain regime and impact biodiversity.
title: Savannization of the Amazon
---


