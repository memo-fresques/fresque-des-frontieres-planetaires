---
backDescription: >-
  Bien qu’il soit très complexe de quantifier les migrations environnementales,
  il est certain que le dépassement de plusieurs frontières planétaires risque
  de conduire de nombreuses populations à quitter leur chez-eux.
title: Réfugiés environnementaux
---

