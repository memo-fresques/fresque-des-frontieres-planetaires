---
backDescription: >-
  Although it is very complex to quantify environmental migrations,
  it is certain that the crossing of several planetary boundaries risks
  leading many populations to leave their homes.
title: Environmental refugees
---


