---
backDescription: >-
  Loss of biodiversity and new chemical pollution can
  cause a decline in the services provided by nature.


  These services make it possible to produce food, fiber and
  materials to human societies.


  They make it possible to regulate the climate, fresh water and nutrients but also
  the fulfillment of cultural, spiritual and aesthetic values.
title: Weakening of services provided by nature
---


