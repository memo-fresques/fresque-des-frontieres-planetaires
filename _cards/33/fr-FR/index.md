---
backDescription: >-
  La perte de biodiversité et les nouvelles pollutions chimiques peuvent
  provoquer la baisse des services rendus par la nature.

  Ces services permettent de produire de la nourriture, des fibres et des
  matériaux aux sociétés humaines.

  Ils permettent de réguler le climat, l'eau douce et les nutriments mais aussi
  l'accomplissement de valeurs culturelles, spirituelles et esthétiques.
title: Affaiblissement des services écosystémiques
---

