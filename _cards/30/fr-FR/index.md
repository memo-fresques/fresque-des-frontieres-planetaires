---
backDescription: >-
  Le changement climatique fait fondre les glaciers qui, avec la modification du
  régime des pluies, fait peser un stress sur la disponibilité en eau douce pour
  de nombreuses populations humaines à travers le globe mais aussi pour la
  biodiversité.
title: Impacts sur les ressources en eau douce
---

L'eau douce ne représente que 3% du volume d'eau total sur la terre. Répartition de cette eau douce : les rivières /fleuves / lacs (1%), les sous-sols (24%) et les glaces (75%).
