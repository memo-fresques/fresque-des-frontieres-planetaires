---
backDescription: >-
  Climate change is melting glaciers which, with the modification of the
  rainy regime, puts stress on the availability of fresh water for
  many human populations across the globe but also for the
  biodiversity.
title: Impacts on freshwater resources
---


Fresh water represents only 3% of the total volume of water on earth. Distribution of this fresh water: rivers / rivers / lakes (1%), basements (24%) and ice (75%).
