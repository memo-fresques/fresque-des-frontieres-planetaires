---
backDescription: >-
  The ocean is necessary for our survival for the services it provides
  (biodiversity, food supply, climate regulator, etc.). Part of the CO2
  emitted into the atmosphere dissolves in the oceans and causes its
  acidification. This process is still in the safe zone, but very close to the
  crossing the border.
title: Ocean acidification
---
On the subject, Alarm Clock Video: https://www.youtube.com/watch?v=vtTlQ0HZZ2g
To calculate the threshold for ocean acidification, we measure the degree of saturation of surface seawater with aragonite, which must not exceed 80% of the pre-industrial value.
