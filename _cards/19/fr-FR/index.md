---
backDescription: >-
  L’océan est nécessaire à notre survie pour les services qu’il rend
  (biodiversité, apport de nourriture, régulateur du climat…). Une partie du CO2
  émis dans l’atmosphère se dissout dans les océans et provoque son
  acidification. Ce processus est encore dans la zone sûre, mais très proche du
  franchissement de la frontière.
title: Acidification des océans
---
Sur le sujet, Vidéo du Réveilleur : https://www.youtube.com/watch?v=vtTlQ0HZZ2g 
Pour calculer le seuil de l’acidification des océans on mesure le degré de saturation de l’eau de mer de surface en aragonite, qui ne doit pas dépasser 80% de la valeur préindustrielle.
