---
backDescription: >-
  Habitat destruction is the leading cause of species extinction and the
  deforestation is the main reason.


  The indicator used to monitor the state of the border "change of use
  of soils" is deforestation, which is 80% linked to agriculture.


  Furthermore, deforestation can profoundly modify the green water cycle.
title: Destruction of natural habitats
---


