---
backDescription: >-
  La destruction des habitats est la première cause d'extinction d'espèces et la
  déforestation en est la raison principale.

  L’indicateur utilisé pour suivre l'état de la frontière "changement d'usage
  des sols" est la déforestation, qui est liée à 80% à l’agriculture.

  Par ailleurs, déforester peut modifier en profondeur le cycle de l’eau verte.
title: Destruction des habitats
---

