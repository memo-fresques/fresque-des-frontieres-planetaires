---
backDescription: >-
  Les aérosols touchent au fonctionnement du système Terre de plusieurs manières
  : formation des nuages, bilan énergétique terrestre....

  La frontière est franchie localement en Asie du sud avec une baisse avérée des
  précipitations, mais le processus se trouve encore en zone sûre au niveau
  mondial.
title: Charge atmosphérique en aérosols
---
"Les aérosols désignent des particules fines en suspension dans l’air. La grande majorité d’entre elles sont d’origine naturelle (éruptions volcaniques, tempêtes de sable, etc.) mais elles peuvent également résulter des activités humaines (aérosols primaires) ou de transformations physico-chimiques dans l’atmosphère (aérosols secondaires).

De par leur taille, les aérosols peuvent pénétrer l’appareil respiratoire et avoir des effets négatifs pour la santé humaine.

Pour appréhender cette limite planétaire, on mesure la concentration globale de particules dans l’atmosphère, sur une base régionale. 
La charge atmosphérique en aérosols est mesurée par le niveau de transmission de la lumière dans l'atmosphère. On utilise le coefficient AOD, profondeur optique d'aérosols, dont la valeur est comprise entre 0 et 1. "
