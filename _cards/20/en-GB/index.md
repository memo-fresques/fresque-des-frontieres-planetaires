---
backDescription: >-
  Aerosols affect the functioning of the Earth system in several ways
  : cloud formation, terrestrial energy balance....


  The border is crossed locally in South Asia with a proven drop in
  precipitation, but the process is still in a safe zone at the level
  world.
title: Atmospheric aerosols loading
---
"Aerosols refer to fine particles suspended in the air. The vast majority of them are of natural origin (volcanic eruptions, sandstorms, etc.) but they can also result from human activities (primary aerosols) or physico-chemical transformations in the atmosphere (secondary aerosols).


Due to their size, aerosols can penetrate the respiratory system and have negative effects on human health.


To understand this planetary boundary, we measure the overall concentration of particles in the atmosphere, on a regional basis.
Atmospheric aerosol load is measured by the level of light transmission in the atmosphere. We use the AOD coefficient, aerosol optical depth, whose value is between 0 and 1.
