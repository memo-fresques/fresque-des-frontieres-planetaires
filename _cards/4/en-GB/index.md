---
backDescription: >-
  Human activities have knocked the Earth system out of its workings
  stable for 10 millennia.


  Humanity is therefore entering the Anthropocene, the epoch of the Earth where the species
  human beings are on par with geological forces.
title: Human activities
---
Human activities have brought the Earth system out of its stable functioning for 10 millennia.
Humanity is therefore entering the Anthropocene, the epoch on Earth where the human species is on an equal footing with geological forces.
