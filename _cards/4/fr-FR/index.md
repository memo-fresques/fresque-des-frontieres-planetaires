---
backDescription: >-
  Les activités humaines ont fait sortir le système Terre de son fonctionnement
  stable depuis 10 millénaires.

  L’humanité entre donc dans l’Anthropocène, l’époque de la Terre où l’espèce
  humaine fait jeu égal avec les forces géologiques.
title: Les activités humaines
---
Les activités humaines ont fait sortir le système Terre de son fonctionnement stable depuis 10 millénaires.
L’humanité entre donc dans l’Anthropocène, l’époque de la Terre où l’espèce humaine fait jeu égal avec les forces géologiques.
