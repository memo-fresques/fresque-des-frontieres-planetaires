---
title: "About Planetary Boundaries Fresco"
---
[The Planetary Boundaries Fresco](https://www.1erdegre.earth/fresque-des-frontieres-planetaires) is a 3-hour collaborative workshop designed to explore different ecological issues: the planetary boundaries.

The Planetary Boundaries Fresco Memo is a tool for facilitators of the Planetary Boundaries Fresco workshop to help them expand or revise their knowledge of the different cards and the links between them.

To [take part in a Fresco of Planetary Frontiers workshop for the general public](https://1erdegre.glide.page/dl/3b1bc8).

To [become a facilitator for the workshop](https://1erdegre.glide.page/dl/dcc150).

To [find out more](https://www.1erdegre.earth/fresque-des-frontieres-planetaires) about The Planetary Boundaries Fresco.

To suggest a change to the explanatory text of the memo for one of the [cards](https://airtable.com/appdFDAoukge42l7u/shrTCs85IwYawwEbS).

To suggest a modification to the explanatory text of the memo for one of the [links](https://airtable.com/appdFDAoukge42l7u/shrPSmo9UY1ICy9th).
