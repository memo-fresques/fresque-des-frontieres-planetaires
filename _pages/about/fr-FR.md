---
title: "À Propos de la Fresque des Frontières Planétaires"
---

[La Fresque des Frontières Planétaires](https://www.1erdegre.earth/fresque-des-frontieres-planetaires) est un atelier collaboratif de 3h permettant d'approfondir différents enjeux écologiques : les frontières planétaires. 

Le mémo de la Fresque des Frontières Planétaires est un outil à destination des animateurs et animatrices de l'atelier Fresque des Frontières Planétaires pour leur permettre d'approfondir ou réviser leurs connaissances sur les différentes cartes, et liens existant entre les cartes. 

Pour [participer à un atelier grand public](https://1erdegre.glide.page/dl/3b1bc8) de la Fresque des Frontières Planétaires.
Pour [se former à l'animation grand public](https://1erdegre.glide.page/dl/dcc150).
Pour en savoir plus sur [La Fresque des Frontières Planétaires](https://www.1erdegre.earth/fresque-des-frontieres-planetaires).

Pour proposer une modification du texte explicatif du mémo pour une des [cartes](https://airtable.com/appdFDAoukge42l7u/shrTCs85IwYawwEbS).
Pour proposer une modification du texte explicatif du mémo pour un des [liens](https://airtable.com/appdFDAoukge42l7u/shrPSmo9UY1ICy9th).
