---
title: "Über Das Puzzle der Planetaren Grenzen"
---
[Das Puzzle der Planetaren Grenzen](https://www.1erdegre.earth/fresque-des-frontieres-planetaires) ist ein dreistündiger Gemeinschaftsworkshop zur Erforschung verschiedener ökologischer Themen: der Planetaren Grenzen.

Das Puzzle der Planetaren Grenzen Memo ist ein Hilfsmittel für Moderatoren Das Puzzle der Planetaren Grenzen, das ihnen hilft, ihr Wissen über die verschiedenen Karten und die Verbindungen zwischen ihnen zu erweitern oder zu überarbeiten.

Um [Moderator für den Workshop zu werden](https://1erdegre.glide.page/dl/3b1bc8).

Um [Moderator für den Workshop zu werden](https://1erdegre.glide.page/dl/dcc150).

Um [mehr über](https://www.1erdegre.earth/fresque-des-frontieres-planetaires) Das Puzzle der Planetaren Grenzen zu erfahren.

Um eine Änderung des erläuternden Texts des Memos für eine der [Karten](https://airtable.com/appdFDAoukge42l7u/shrTCs85IwYawwEbS) vorzuschlagen.

Um eine Änderung des erläuternden Texts des Memos für einen der [Links](https://airtable.com/appdFDAoukge42l7u/shrPSmo9UY1ICy9th) vorzuschlagen.
